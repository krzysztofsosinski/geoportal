package pl.geoportal.persistence;

import pl.geoportal.model.RoutesEntity;
import pl.geoportal.model.RoutesTracksEntity;
import pl.geoportal.persistence.DTO.SimplifiedRoutesTrack;

import java.util.List;

public interface RoutesTracksDao{

    List<RoutesTracksEntity> getByRoute(RoutesEntity routesEntity);

    List<SimplifiedRoutesTrack> getTracks();
}
