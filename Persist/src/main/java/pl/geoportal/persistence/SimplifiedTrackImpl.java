package pl.geoportal.persistence;

import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import pl.geoportal.model.SimplifiedTracksEntity;

import java.util.List;

@Repository
public class SimplifiedTrackImpl extends BaseDao implements SimplifiedTrackDao {

    @Override
    @SuppressWarnings("unchecked")
    public List<SimplifiedTracksEntity> getByRouteAndDirection(Integer routeId, Integer direction) {
        return (List<SimplifiedTracksEntity>) getCurrentSession().createCriteria(SimplifiedTracksEntity.class)
                .add(Restrictions.eq("route.routeId", routeId))
                .add(Restrictions.eq("direction", direction))
                .addOrder(Order.asc("trackOrder"))
                .list();
    }
}
