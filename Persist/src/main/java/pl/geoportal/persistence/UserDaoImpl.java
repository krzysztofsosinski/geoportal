package pl.geoportal.persistence;

import org.springframework.stereotype.Repository;
import pl.geoportal.model.UserEntity;

import java.util.List;

@Repository
public class UserDaoImpl extends BaseDao implements UserDao {

    @Override
    public void save(UserEntity p) {
        getCurrentSession().persist(p);
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<UserEntity> list() {
        return getCurrentSession().createQuery("from UserEntity").list();
    }

    @Override
    public UserEntity findByEmail(String email) {
        return (UserEntity) getCurrentSession()
                .getNamedQuery("findUserByEmail")
                .setString("email", email).uniqueResult();
    }
}
