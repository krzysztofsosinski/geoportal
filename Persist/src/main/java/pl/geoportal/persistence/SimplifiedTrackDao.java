package pl.geoportal.persistence;

import pl.geoportal.model.SimplifiedTracksEntity;

import java.util.List;

public interface SimplifiedTrackDao {

    List<SimplifiedTracksEntity> getByRouteAndDirection(Integer routeId, Integer direction);
}
