package pl.geoportal.persistence.DTO;

public class TimetableKey {
    public Integer busStopId;
    public Integer serviceId;

    public TimetableKey(Integer busStopId, Integer serviceId) {
        this.busStopId = busStopId;
        this.serviceId = serviceId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TimetableKey that = (TimetableKey) o;

        if (busStopId != null ? !busStopId.equals(that.busStopId) : that.busStopId != null) return false;
        return !(serviceId != null ? !serviceId.equals(that.serviceId) : that.serviceId != null);

    }

    @Override
    public int hashCode() {
        int result = busStopId != null ? busStopId.hashCode() : 0;
        result = 31 * result + (serviceId != null ? serviceId.hashCode() : 0);
        return result;
    }
}
