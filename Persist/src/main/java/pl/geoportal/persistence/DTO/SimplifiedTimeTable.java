package pl.geoportal.persistence.DTO;

import lombok.Data;

import java.sql.Time;

@Data
public class SimplifiedTimeTable {

    Integer id;
    Time departure;
    Integer direction;
    Integer routeId;
    Integer serviceId;
    Integer calendarId;
    Integer busStopGid;
    String description;
}
