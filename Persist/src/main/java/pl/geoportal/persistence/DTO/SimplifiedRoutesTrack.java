package pl.geoportal.persistence.DTO;

import lombok.Data;

@Data
public class SimplifiedRoutesTrack {

    private Integer id;
    private Integer routeId;
    private Integer type;
    private Integer orderNumber;
    private Integer busStopGid;


}
