package pl.geoportal.persistence;

import com.vividsolutions.jts.geom.Point;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.spatial.criterion.SpatialRestrictions;
import org.springframework.stereotype.Repository;
import pl.geoportal.model.BusStopsEntity;
import pl.geoportal.model.StopTypeEntity;

import java.util.List;


@Repository
public class BusStopsDaoImpl extends BaseDao implements BusStopsDao {

    @Override
    public void save(BusStopsEntity busStopsEntity) {
        getCurrentSession().saveOrUpdate(busStopsEntity);
    }

    @Override
    public BusStopsEntity checkIfPointExists(Point point) {
        Criteria criteria = getCurrentSession().createCriteria(BusStopsEntity.class);
        criteria.add(SpatialRestrictions.eq("geom", point));
        return (BusStopsEntity) criteria.uniqueResult();
    }

    @Override
    public StopTypeEntity getType(Integer id){
        return (StopTypeEntity) getCurrentSession().load(StopTypeEntity.class, id);
    }

    @Override
    @SuppressWarnings("unchecked")
    public BusStopsEntity getByName(String name) {
        Criteria criteria = getCurrentSession().createCriteria(BusStopsEntity.class);
        criteria.add(Restrictions.eq("name", name));
        return (BusStopsEntity) criteria.uniqueResult();
    }


    @Override
    @SuppressWarnings("unchecked")
    public List<BusStopsEntity> searchByName(String needle, Integer max){
        return (List<BusStopsEntity>) getCurrentSession().createCriteria(BusStopsEntity.class)
                .add(Restrictions.ilike("name", needle, MatchMode.START))
                .setMaxResults(max)
                .list();
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<BusStopsEntity> getNullGeoms() {
        Query query = getCurrentSession().createQuery("from BusStopsEntity where geom is null");
        return (List<BusStopsEntity>) query.list();
    }

    @Override
    public Integer isExist(String name) {
        Criteria criteria = getCurrentSession().createCriteria(BusStopsEntity.class);
        criteria.add(Restrictions.eq("name", name));
        criteria.setProjection(Projections.property("gid"));
        return (Integer) criteria.uniqueResult();
    }
}
