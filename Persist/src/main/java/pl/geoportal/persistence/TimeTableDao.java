package pl.geoportal.persistence;

import pl.geoportal.persistence.DTO.SimplifiedTimeTable;
import pl.geoportal.persistence.DTO.TimetableKey;

import java.sql.Time;
import java.util.List;
import java.util.Map;

public interface TimeTableDao {

    List<SimplifiedTimeTable> findByRoutesAndCalendar(List<Integer> routes, Integer calendarId, Time departure);
    Map<TimetableKey, List<SimplifiedTimeTable> > getAll();
    List<SimplifiedTimeTable> getAllByBusId(Integer gid, Integer sid);
    List<Time> getTimes(Integer busId, Integer sid, Integer direction, Integer routeId);
}
