package pl.geoportal.persistence;

import pl.geoportal.model.UserEntity;

import java.util.List;

public interface UserDao {

    public void save(UserEntity p);

    public List list();

    UserEntity findByEmail(String email);
}
