package pl.geoportal.persistence;

import org.hibernate.transform.ResultTransformer;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

public interface GenericDao<Entity, ID extends Serializable> {

    Entity getByID(ID id);

    List getList();

    public <T> List<T> getAll(final Class<T> type);

    List<Entity> search(Map<String, Object> parameterMap);

    List<Entity> searchByEq(Map<String, Object> parameterMap);

    ID insert(Entity entity);

    <T> void persist(T object);

    <T> void deleteN(T entity);

    void update(Entity entity);

    void delete(Entity entity);

    void deleteById(ID id);

    public void senEntityClass(final Class clazz);

    public void runNativeQuery(String query, Map<String,Object> parameters);

    List runNativeQuery(String query, Map<String,Object> parameters, ResultTransformer transformer);

    List runNamedQuery(String query, Map<String, Object> parameters);

    List runNamedQuery(String query, Map<String, Object> parameters, ResultTransformer transformer);
}
