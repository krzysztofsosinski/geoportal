package pl.geoportal.persistence;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Repository;
import pl.geoportal.model.TimetableEntity;
import pl.geoportal.persistence.DTO.SimplifiedTimeTable;
import pl.geoportal.persistence.DTO.TimetableKey;

import java.sql.Time;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class TimeTableDaoImpl extends BaseDao implements TimeTableDao {


    @Override
    @SuppressWarnings("unchecked")
    public List<SimplifiedTimeTable> findByRoutesAndCalendar(List<Integer> busStops, Integer calendarId, Time departure) {
        ProjectionList projectionList = Projections.projectionList()
                .add(Projections.property("id"), "id")
                .add(Projections.property("departure"), "departure")
                .add(Projections.property("direction"), "direction")
                .add(Projections.property("route.routeId"), "routeId")
                .add(Projections.property("calendar.serviceId"), "serviceId")
                .add(Projections.property("busStop.gid"), "busStopGid")
                .add(Projections.property("description"), "description");

        Criteria criteria = getCurrentSession().createCriteria(TimetableEntity.class);
        criteria
                .add(Restrictions.eq("calendar.serviceId", calendarId))
                .add(Restrictions.in("busStop.gid", busStops))
                .add(Restrictions.gt("departure", departure))
                .setFetchMode("busStop", FetchMode.JOIN)
                .addOrder(Order.asc("departure"))
                .setProjection(projectionList)
                .setResultTransformer(Transformers.aliasToBean(SimplifiedTimeTable.class));

        return criteria.list();
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<SimplifiedTimeTable> getAllByBusId(Integer gid, Integer sid) {
        ProjectionList projectionList = Projections.projectionList()
                .add(Projections.property("departure"), "departure")
                .add(Projections.property("direction"), "direction")
                .add(Projections.property("route.routeId"), "routeId")
                .add(Projections.property("busStop.gid"), "busStopGid")
                .add(Projections.property("calendar.serviceId"), "serviceId");

        Criteria criteria = getCurrentSession().createCriteria(TimetableEntity.class);
        criteria
                .add(Restrictions.eq("busStop.gid", gid))
                .add(Restrictions.eq("calendar.serviceId", sid))
                .addOrder(Order.asc("departure"))
                .setProjection(projectionList)
                .setResultTransformer(Transformers.aliasToBean(SimplifiedTimeTable.class));

        return criteria.list();
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Time> getTimes(Integer busId, Integer sid, Integer direction, Integer routeId) {
        ProjectionList projectionList = Projections.projectionList()
                .add(Projections.property("departure"), "departure");

        Criteria criteria = getCurrentSession().createCriteria(TimetableEntity.class);
        criteria
                .add(Restrictions.eq("busStop.gid", busId))
                .add(Restrictions.eq("calendar.serviceId", sid))
                .add(Restrictions.eq("direction", direction))
                .add(Restrictions.eq("route.routeId", routeId))
                .addOrder(Order.asc("departure"))
                .setMaxResults(10)
                .setProjection(projectionList);
        return criteria.list();
    }

    @Override
    @SuppressWarnings("unchecked")
    @Cacheable(value = "timetable")
    public Map<TimetableKey, List<SimplifiedTimeTable>> getAll() {

        List<Integer> busIds = getCurrentSession().createQuery("SELECT gid FROM BusStopsEntity").list();

        Map<TimetableKey, List<SimplifiedTimeTable>> result = new HashMap<>();
        Integer[] sids = new Integer[]{1, 2, 3};
        for (Integer gid : busIds) {
            for (Integer sid : sids) {
                List<SimplifiedTimeTable> times = getAllByBusId(gid, sid);
                if (times.isEmpty()) continue;
                TimetableKey timetableKey = new TimetableKey(gid, sid);
                result.put(timetableKey, times);
            }
        }


        return result;
    }

}
