package pl.geoportal.persistence;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Repository;
import pl.geoportal.model.RoutesEntity;
import pl.geoportal.model.RoutesTracksEntity;
import pl.geoportal.persistence.DTO.SimplifiedRoutesTrack;

import java.util.List;

@Repository
public class RoutesTracksDaoImpl extends BaseDao implements RoutesTracksDao {

    @Override
    @SuppressWarnings("unchecked")
    public List<RoutesTracksEntity> getByRoute(RoutesEntity routesEntity) {
        Criteria criteria = getCurrentSession().createCriteria(RoutesTracksEntity.class);
        criteria
                .add(Restrictions.eq("route", routesEntity))
                .add(Restrictions.gt("orderNumber",-1))
                .addOrder(Order.asc("orderNumber"));
        return criteria.list();
    }

    @Override
    @Cacheable("tracks")
    @SuppressWarnings("unchecked")
    public List<SimplifiedRoutesTrack> getTracks() {
        Criteria criteria = getCurrentSession().createCriteria(RoutesTracksEntity.class);
        ProjectionList projectionList = Projections.projectionList()
                .add(Projections.property("id"), "id")
                .add(Projections.property("route.routeId"), "routeId")
                .add(Projections.property("type"), "type")
                .add(Projections.property("orderNumber"), "orderNumber")
                .add(Projections.property("busStop.gid"), "busStopGid")
                ;
        criteria
                .setProjection(projectionList)
//                .setFetchMode("route", FetchMode.JOIN)
                .add(Restrictions.gt("orderNumber", -1))
//                .addOrder(Order.asc("id"))
//                .addOrder(Order.asc("type"))
//                .addOrder(Order.asc("orderNumber"))
                .setResultTransformer(Transformers.aliasToBean(SimplifiedRoutesTrack.class));

        return criteria.list();
    }
}
