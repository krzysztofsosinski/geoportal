package pl.geoportal.persistence;

import pl.geoportal.model.RoutesEntity;

import java.util.List;

public interface RoutesDao {

    List<RoutesEntity> searchByName(String needle, Integer max);
}
