package pl.geoportal.persistence;

import com.vividsolutions.jts.geom.Point;
import pl.geoportal.model.BusStopsEntity;
import pl.geoportal.model.StopTypeEntity;

import java.util.List;

public interface BusStopsDao {

    public void save(BusStopsEntity busStopsEntity);
    public BusStopsEntity checkIfPointExists(Point point);
    public StopTypeEntity getType(Integer id);
    public BusStopsEntity getByName(String name);
    List<BusStopsEntity> searchByName(String needle, Integer max);
    public List<BusStopsEntity> getNullGeoms();
    public Integer isExist(String name);
}
