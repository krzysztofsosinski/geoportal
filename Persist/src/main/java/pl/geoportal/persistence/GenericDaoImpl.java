package pl.geoportal.persistence;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.ResultTransformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Set;

@SuppressWarnings(value = "unchecked")
@Repository
public class GenericDaoImpl<Entity, ID extends Serializable> implements GenericDao<Entity, ID> {

    protected Class<Entity> clazz;

    @Autowired
    SessionFactory sessionFactory;

//    public GenericDaoImpl(){
//        this.clazz = (Class<Entity>) ((ParameterizedType) getClass()
//                .getGenericSuperclass()).getActualTypeArguments()[0];
//    }

    public void senEntityClass(final Class clazz){
        this.clazz = clazz;
    }

    @Override
    public void runNativeQuery(String query, Map<String, Object> parameters) {
        Query q = getCurrentSession().createSQLQuery(query);
        for(Map.Entry<String, Object> entry : parameters.entrySet()){
            q.setParameter(entry.getKey(), entry.getValue());
        }
        q.executeUpdate();
    }

    @Override
    public List runNativeQuery(String query, Map<String, Object> parameters, ResultTransformer transformer) {
        Query q = getCurrentSession().createSQLQuery(query);
        q.setResultTransformer(transformer);
        for(Map.Entry<String, Object> entry : parameters.entrySet()){
            q.setParameter(entry.getKey(), entry.getValue());
        }
        return q.list();
    }

    @Override
    public List runNamedQuery(String query, Map<String, Object> parameters) {
        Query q = getCurrentSession().getNamedQuery(query);
        for(Map.Entry<String, Object> entry : parameters.entrySet()){
            if(entry.getValue() instanceof List) {
                q.setParameterList(entry.getKey(),(List) entry.getValue());
            }else{
                q.setParameter(entry.getKey(), entry.getValue());
            }
        }

        return q.list();
    }

    @Override
    public List runNamedQuery(String query, Map<String, Object> parameters, ResultTransformer transformer) {
        Query q = getCurrentSession().getNamedQuery(query);
        for(Map.Entry<String, Object> entry : parameters.entrySet()){
            if(entry.getValue() instanceof List) {
                q.setParameterList(entry.getKey(),(List) entry.getValue());
            }else{
                q.setParameter(entry.getKey(), entry.getValue());
            }
        }
        q.setResultTransformer(transformer);
        return q.list();
    }

    @Override
    public Entity getByID(ID id) {
        return (Entity) getCurrentSession().get(clazz, id);
    }

    @Override
    public <T> List<T> getAll(Class<T> type) {
        return sessionFactory
                .getCurrentSession()
                .createCriteria(type)
                .list();
    }

    @Override
    public List getList() {
        return getCurrentSession().createQuery("from " + clazz.getName())
                .list();
    }

    @Override
    public List<Entity> search( Map<String, Object> parameterMap ) {
        Criteria criteria = getCurrentSession().createCriteria(clazz);
        Set<String> fieldName = parameterMap.keySet();
        for (String field : fieldName) {
            criteria.add(Restrictions.ilike(field, parameterMap.get(field)));
        }
        return criteria.list();
    }

    @Override
    public List<Entity> searchByEq(Map<String, Object> parameterMap) {
        Criteria criteria = getCurrentSession().createCriteria(clazz);
        Set<String> fieldName = parameterMap.keySet();
        for(String field : fieldName){
            criteria.add(Restrictions.eq(field, parameterMap.get(field)));
        }
        return criteria.list();
    }

    @Override
    public ID insert(Entity entity) {
        return (ID) getCurrentSession().save(entity);
    }

    @Override
    public <T> void persist(T object){
        getCurrentSession().saveOrUpdate(object);
    }

    @Override
    public <T> void deleteN(T entity) {
        getCurrentSession().delete(entity);
    }

    @Override
    public void update(Entity entity) {
        getCurrentSession().saveOrUpdate(entity);
    }

    @Override
    public void delete(Entity entity) {
        getCurrentSession().delete(entity);
    }

    @Override
    public void deleteById(ID id) {
        delete(getByID(id));
    }

    protected Session getCurrentSession(){
        return sessionFactory.getCurrentSession();
    }
}
