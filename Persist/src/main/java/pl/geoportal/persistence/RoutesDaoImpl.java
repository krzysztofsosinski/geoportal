package pl.geoportal.persistence;

import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import pl.geoportal.model.RoutesEntity;

import java.util.List;

@Repository
public class RoutesDaoImpl extends BaseDao implements RoutesDao {

    @Override
    @SuppressWarnings("unchecked")
    public List<RoutesEntity> searchByName(String needle, Integer max) {
        return (List<RoutesEntity>) getCurrentSession().createCriteria(RoutesEntity.class)
                .add(Restrictions.ilike("routeName", needle, MatchMode.START))
                .setMaxResults(max)
                .list();
    }
}
