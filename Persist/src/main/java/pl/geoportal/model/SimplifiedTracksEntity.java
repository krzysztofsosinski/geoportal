package pl.geoportal.model;

import javax.persistence.*;
import java.sql.Time;


@Entity
@Table(name = "simplified_tracks", schema = "geoportal", catalog = "kzkgop")
public class SimplifiedTracksEntity {
    private Integer id;
    private Integer trackOrder;
    private Integer direction;
    private Time departure;
    private BusStopsEntity busStop;
    private RoutesEntity route;

    @Id
    @Column(name = "id")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "track_order")
    public Integer getTrackOrder() {
        return trackOrder;
    }

    public void setTrackOrder(Integer trackOrder) {
        this.trackOrder = trackOrder;
    }

    @Basic
    @Column(name = "direction")
    public Integer getDirection() {
        return direction;
    }

    public void setDirection(Integer direction) {
        this.direction = direction;
    }

    @Basic
    @Column(name = "departure")
    public Time getDeparture() {
        return departure;
    }

    public void setDeparture(Time departure) {
        this.departure = departure;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "bus_stop_id", referencedColumnName = "`gid`", nullable = false)
    public BusStopsEntity getBusStop() {
        return busStop;
    }

    public void setBusStop(BusStopsEntity busStop) {
        this.busStop = busStop;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "route_id", referencedColumnName = "`route_id`", nullable = false)
    public RoutesEntity getRoute() {
        return route;
    }

    public void setRoute(RoutesEntity route) {
        this.route = route;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SimplifiedTracksEntity that = (SimplifiedTracksEntity) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (trackOrder != null ? !trackOrder.equals(that.trackOrder) : that.trackOrder != null) return false;
        if (direction != null ? !direction.equals(that.direction) : that.direction != null) return false;
        if (departure != null ? !departure.equals(that.departure) : that.departure != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (trackOrder != null ? trackOrder.hashCode() : 0);
        result = 31 * result + (direction != null ? direction.hashCode() : 0);
        result = 31 * result + (departure != null ? departure.hashCode() : 0);
        return result;
    }
}
