package pl.geoportal.model;

public enum Role {
    ROLE_ADMIN, ROLE_USER
}
