package pl.geoportal.model;

import com.vividsolutions.jts.geom.LineString;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.Collection;

@Entity
@Table(name = "routes", schema = "geoportal", catalog = "kzkgop")
public class RoutesEntity {
    private Integer routeId;
    private String routeName;
    private String routeDesc;
    private Short routeType;
    private LineString geom;
    private Collection<TripsEntity> tripsesByRouteId;

    public RoutesEntity(){}

    public RoutesEntity(String routeName) {
        this.routeName = routeName;
    }

    public RoutesEntity(Short routeType, String routeName) {
        this.routeType = routeType;
        this.routeName = routeName;
    }

    public RoutesEntity(String routeName, String routeDesc, Short routeType) {
        this(routeType, routeName);
        this.routeType = routeType;
    }

    @Basic
    @Type(type = "org.hibernate.spatial.GeometryType")
    @Column(name = "geom", nullable = true)
    public LineString getGeom() {
        return geom;
    }

    public void setGeom(LineString geom) {
        this.geom = geom;
    }

    @Id
    @SequenceGenerator(name="routes_sequence",
            sequenceName="routes_route_id_seq",
            allocationSize=1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE,
            generator="routes_sequence")
    @Column(name = "route_id")
    public Integer getRouteId() {
        return routeId;
    }

    public void setRouteId(Integer routeId) {
        this.routeId = routeId;
    }

    @Basic
    @Column(name = "route_name")
    public String getRouteName() {
        return routeName;
    }

    public void setRouteName(String routeName) {
        this.routeName = routeName;
    }

    @Basic
    @Column(name = "route_desc")
    public String getRouteDesc() {
        return routeDesc;
    }

    public void setRouteDesc(String routeDesc) {
        this.routeDesc = routeDesc;
    }

    @Basic
    @Column(name = "route_type")
    public Short getRouteType() {
        return routeType;
    }

    public void setRouteType(Short routeType) {
        this.routeType = routeType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RoutesEntity that = (RoutesEntity) o;

        if (routeId != null ? !routeId.equals(that.routeId) : that.routeId != null) return false;
        if (!routeName.equals(that.routeName)) return false;
        if (routeDesc != null ? !routeDesc.equals(that.routeDesc) : that.routeDesc != null) return false;
        if (!routeType.equals(that.routeType)) return false;
        if (geom != null ? !geom.equals(that.geom) : that.geom != null) return false;
        return !(tripsesByRouteId != null ? !tripsesByRouteId.equals(that.tripsesByRouteId) : that.tripsesByRouteId != null);

    }

    @Override
    public int hashCode() {
        int result = routeId != null ? routeId.hashCode() : 0;
        result = 31 * result + routeName.hashCode();
        result = 31 * result + (routeDesc != null ? routeDesc.hashCode() : 0);
        result = 31 * result + routeType.hashCode();
        result = 31 * result + (geom != null ? geom.hashCode() : 0);
        result = 31 * result + (tripsesByRouteId != null ? tripsesByRouteId.hashCode() : 0);
        return result;
    }

    @OneToMany(mappedBy = "routesByRouteId", cascade = {CascadeType.ALL, CascadeType.MERGE, CascadeType.PERSIST})
    public Collection<TripsEntity> getTripsesByRouteId() {
        return tripsesByRouteId;
    }

    public void setTripsesByRouteId(Collection<TripsEntity> tripsesByRouteId) {
        this.tripsesByRouteId = tripsesByRouteId;
    }
}
