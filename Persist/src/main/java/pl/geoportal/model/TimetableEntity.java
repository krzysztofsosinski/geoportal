package pl.geoportal.model;

import javax.persistence.*;
import java.sql.Time;


@NamedQueries({
        @NamedQuery(
                name = "findTimetablesByBusId",
                query = "SELECT id, departure, direction, route.routeId as routeId, calendar.serviceId as calendarId, busStop.gid as busStopGid, description " +
                        "from TimetableEntity WHERE busStop.gid IN (:stop_id) AND calendar.serviceId = :service_id"
        )
})
@Entity
@Table(name = "timetable", schema = "geoportal", catalog = "kzkgop")
public class TimetableEntity {
    private Integer id;
    private Time departure;
    private Integer direction;
    private String description;
    private String annotation;
    private BusStopsEntity busStop;
    private RoutesEntity route;
    private CalendarEntity calendar;

    public TimetableEntity() {
    }

    public TimetableEntity(Time departure, Integer direction, String description, String annotation, BusStopsEntity busStop, RoutesEntity route) {
        this.departure = departure;
        this.direction = direction;
        this.description = description;
        this.annotation = annotation;
        this.busStop = busStop;
        this.route = route;
    }

    public TimetableEntity(Time departure, Integer direction, String description, String annotation, BusStopsEntity busStop, RoutesEntity route, CalendarEntity calendar) {
        this.departure = departure;
        this.direction = direction;
        this.description = description;
        this.annotation = annotation;
        this.busStop = busStop;
        this.route = route;
        this.calendar = calendar;
    }

    @Id
    @SequenceGenerator(name = "timetable_sequence",
            sequenceName = "timetable_id_seq",
            allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE,
            generator = "timetable_sequence")
    @Column(name = "id")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "departure")
    public Time getDeparture() {
        return departure;
    }

    public void setDeparture(Time departure) {
        this.departure = departure;
    }

    @Basic
    @Column(name = "direction")
    public Integer getDirection() {
        return direction;
    }

    public void setDirection(Integer direction) {
        this.direction = direction;
    }

    @Basic
    @Column(name = "description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Basic
    @Column(name = "annotation")
    public String getAnnotation() {
        return annotation;
    }

    public void setAnnotation(String annotation) {
        this.annotation = annotation;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "bus_stop_id", referencedColumnName = "`gid`", nullable = false)
    public BusStopsEntity getBusStop() {
        return busStop;
    }

    public void setBusStop(BusStopsEntity busStop) {
        this.busStop = busStop;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "route_id", referencedColumnName = "`route_id`", nullable = false)
    public RoutesEntity getRoute() {
        return route;
    }

    public void setRoute(RoutesEntity route) {
        this.route = route;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "service_id", referencedColumnName = "`service_id`", nullable = false)
    public CalendarEntity getCalendar() {
        return calendar;
    }

    public void setCalendar(CalendarEntity calendar) {
        this.calendar = calendar;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TimetableEntity that = (TimetableEntity) o;

        if (annotation != null ? !annotation.equals(that.annotation) : that.annotation != null) return false;
        if (busStop != null ? !busStop.equals(that.busStop) : that.busStop != null) return false;
        if (calendar != null ? !calendar.equals(that.calendar) : that.calendar != null) return false;
        if (departure != null ? !departure.equals(that.departure) : that.departure != null) return false;
        if (description != null ? !description.equals(that.description) : that.description != null) return false;
        if (direction != null ? !direction.equals(that.direction) : that.direction != null) return false;
        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (route != null ? !route.equals(that.route) : that.route != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (departure != null ? departure.hashCode() : 0);
        result = 31 * result + (direction != null ? direction.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (annotation != null ? annotation.hashCode() : 0);
        result = 31 * result + (busStop != null ? busStop.hashCode() : 0);
        result = 31 * result + (route != null ? route.hashCode() : 0);
        result = 31 * result + (calendar != null ? calendar.hashCode() : 0);
        return result;
    }
}
