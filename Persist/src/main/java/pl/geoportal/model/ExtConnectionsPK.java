package pl.geoportal.model;


import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;
import java.io.Serializable;

@Embeddable
public class ExtConnectionsPK implements Serializable{
    private BusStopsEntity start;
    private BusStopsEntity end;
    private Integer direction;
    private RoutesEntity route;

    @ManyToOne
    public BusStopsEntity getStart() {
        return start;
    }

    public void setStart(BusStopsEntity start) {
        this.start = start;
    }

    @ManyToOne
    public BusStopsEntity getEnd() {
        return end;
    }

    public void setEnd(BusStopsEntity end) {
        this.end = end;
    }

    public Integer getDirection() {
        return direction;
    }

    public void setDirection(Integer direction) {
        this.direction = direction;
    }

    @ManyToOne
    public RoutesEntity getRoute() {
        return route;
    }

    public void setRoute(RoutesEntity route) {
        this.route = route;
    }
}
