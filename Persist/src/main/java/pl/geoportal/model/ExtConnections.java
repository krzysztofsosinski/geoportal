package pl.geoportal.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(schema = "geoportal", name = "ext_connections", catalog = "kzkgop")
@AssociationOverrides({
        @AssociationOverride(name = "pk.start",
                joinColumns = @JoinColumn(name = "start_point")),
        @AssociationOverride(name = "pk.end",
                joinColumns = @JoinColumn(name = "end_point")),
        @AssociationOverride(name = "pk.route",
                joinColumns = @JoinColumn(name = "route_id"))
})
public class ExtConnections implements Serializable {

    private ExtConnectionsPK pk;

    @EmbeddedId
    public ExtConnectionsPK getPk() {
        return pk;
    }

    public void setPk(ExtConnectionsPK pk) {
        this.pk = pk;
    }
}
