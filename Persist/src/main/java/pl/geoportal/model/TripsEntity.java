package pl.geoportal.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;

@Entity
@Table(name = "trips", schema = "geoportal", catalog = "kzkgop")
public class TripsEntity {
    private Integer tripId;
    private Short directionId;
    private String tripName;
    private String tripHeadsign;
    private Collection<StopTimesEntity> stopTimesesByTripId;
    private RoutesEntity routesByRouteId;
    private CalendarEntity calendarEntity;

    public TripsEntity(){}

    public TripsEntity(Short directionId, String tripName, String tripHeadsign,
                       RoutesEntity routesByRouteId, CalendarEntity calendarEntity) {
        this.directionId = directionId;
        this.tripName = tripName;
        this.tripHeadsign = tripHeadsign;
        this.routesByRouteId = routesByRouteId;
        this.calendarEntity = calendarEntity;
        this.stopTimesesByTripId = new ArrayList<>();
    }

    @Id
    @SequenceGenerator(name="trips_sequence",
            sequenceName="trips_trip_id_seq",
            allocationSize=1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE,
            generator="trips_sequence")
    @Column(name = "trip_id")
    public Integer getTripId() {
        return tripId;
    }

    public void setTripId(Integer tripId) {
        this.tripId = tripId;
    }

    @Basic
    @Column(name = "direction_id")
    public Short getDirectionId() {
        return directionId;
    }

    public void setDirectionId(Short directionId) {
        this.directionId = directionId;
    }

    @Basic
    @Column(name = "trip_name")
    public String getTripName() {
        return tripName;
    }

    public void setTripName(String tripName) {
        this.tripName = tripName;
    }

    @Basic
    @Column(name = "trip_headsign")
    public String getTripHeadsign() {
        return tripHeadsign;
    }

    public void setTripHeadsign(String tripHeadsign) {
        this.tripHeadsign = tripHeadsign;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TripsEntity that = (TripsEntity) o;

        if (directionId != null ? !directionId.equals(that.directionId) : that.directionId != null) return false;
        if (tripHeadsign != null ? !tripHeadsign.equals(that.tripHeadsign) : that.tripHeadsign != null) return false;
        if (tripId != null ? !tripId.equals(that.tripId) : that.tripId != null) return false;
        if (tripName != null ? !tripName.equals(that.tripName) : that.tripName != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = tripId != null ? tripId.hashCode() : 0;
        result = 31 * result + (directionId != null ? directionId.hashCode() : 0);
        result = 31 * result + (tripName != null ? tripName.hashCode() : 0);
        result = 31 * result + (tripHeadsign != null ? tripHeadsign.hashCode() : 0);
        return result;
    }

    @OneToMany(mappedBy = "tripsByTripId", cascade = {CascadeType.ALL, CascadeType.MERGE, CascadeType.PERSIST})
    public Collection<StopTimesEntity> getStopTimesesByTripId() {
        return stopTimesesByTripId;
    }

    public void setStopTimesesByTripId(Collection<StopTimesEntity> stopTimesesByTripId) {
        this.stopTimesesByTripId = stopTimesesByTripId;
    }

    @ManyToOne
    @JoinColumn(name = "route_id", referencedColumnName = "`route_id`", nullable = false)
    public RoutesEntity getRoutesByRouteId() {
        return routesByRouteId;
    }

    public void setRoutesByRouteId(RoutesEntity routesByRouteId) {
        this.routesByRouteId = routesByRouteId;
    }

    @ManyToOne
    @JoinColumn(name ="service_id", referencedColumnName = "`service_id`", nullable = false)
    public CalendarEntity getCalendarEntity() {
        return calendarEntity;
    }

    public void setCalendarEntity(CalendarEntity calendarEntity) {
        this.calendarEntity = calendarEntity;
    }
}
