package pl.geoportal.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;
import java.io.Serializable;

@Embeddable
public class ConnectionsPK implements Serializable {

    private BusStopsEntity start;
    private BusStopsEntity end;
    private Integer direction;

    @ManyToOne
    public BusStopsEntity getStart() {
        return start;
    }

    public void setStart(BusStopsEntity start) {
        this.start = start;
    }

    @ManyToOne
    public BusStopsEntity getEnd() {
        return end;
    }

    public void setEnd(BusStopsEntity end) {
        this.end = end;
    }

    @Basic
    @Column(name = "direction", nullable = false)
    public Integer getDirection() {
        return direction;
    }

    public void setDirection(Integer direction) {
        this.direction = direction;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ConnectionsPK that = (ConnectionsPK) o;

        if (!start.equals(that.start)) return false;
        if (!end.equals(that.end)) return false;
        return direction.equals(that.direction);

    }

    @Override
    public int hashCode() {
        int result = start.hashCode();
        result = 31 * result + end.hashCode();
        result = 31 * result + direction.hashCode();
        return result;
    }
}
