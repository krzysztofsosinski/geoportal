package pl.geoportal.model;

import javax.persistence.*;

@Entity
@Table(name = "temp_links", schema = "geoportal", catalog = "kzkgop")
public class TempLinksEntity {
    private Integer id;
    private String calendar;
    private String url;
    private String route;
    private Integer direction;

    public TempLinksEntity(){}

    public TempLinksEntity(String calendar, String url, String route, Integer direction) {
        this.calendar = calendar;
        this.url = url;
        this.route = route;
        this.direction = direction;
    }

    @Id
    @SequenceGenerator(name="temp_links_sequence",
            sequenceName="temp_links_id_seq",
            allocationSize=1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE,
            generator="temp_links_sequence")
    @Column(name = "id")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "calendar")
    public String getCalendar() {
        return calendar;
    }

    public void setCalendar(String calendar) {
        this.calendar = calendar;
    }

    @Basic
    @Column(name = "url")
    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Basic
    @Column(name = "direction")
    public Integer getDirection() {
        return direction;
    }

    public void setDirection(Integer route) {
        this.direction = route;
    }

    @Basic
    @Column(name = "route")
    public String getRoute() {
        return route;
    }

    public void setRoute(String route) {
        this.route = route;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TempLinksEntity that = (TempLinksEntity) o;

        if (calendar != null ? !calendar.equals(that.calendar) : that.calendar != null) return false;
        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (route != null ? !route.equals(that.route) : that.route != null) return false;
        if (url != null ? !url.equals(that.url) : that.url != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (calendar != null ? calendar.hashCode() : 0);
        result = 31 * result + (url != null ? url.hashCode() : 0);
        result = 31 * result + (route != null ? route.hashCode() : 0);
        return result;
    }
}
