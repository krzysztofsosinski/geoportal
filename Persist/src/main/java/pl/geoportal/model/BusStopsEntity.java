package pl.geoportal.model;

import com.vividsolutions.jts.geom.Point;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "bus_stops", schema = "geoportal", catalog = "kzkgop")
public class BusStopsEntity {
    private Integer gid;
    private String name;
    private Integer communityId;
    private Point geom;
    private StopTypeEntity stopTypeByType;
    private List<TimetableEntity> times;
    private Set<Connections> stopsFrom;
    private Set<Connections> stopsTo;

    private Set<ExtConnections> extStopsFrom;
    private Set<ExtConnections> extStopsTO;


    public BusStopsEntity(){}

    public BusStopsEntity(String name, Integer communityId, StopTypeEntity type) {
        this.name = name;
        this.communityId = communityId;
        this.stopTypeByType = type;
    }

    @Id
    @SequenceGenerator(name="bus_stops_sequence",
            sequenceName="bus_stops_gid_seq",
            allocationSize=1)
    @GeneratedValue(strategy = GenerationType.IDENTITY,
            generator="bus_stops_sequence")
    @Column(name = "gid")
    public Integer getGid() {
        return gid;
    }

    public void setGid(Integer gid) {
        this.gid = gid;
    }

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "community_id")
    public Integer getCommunityId() {
        return communityId;
    }

    public void setCommunityId(Integer communityId) {
        this.communityId = communityId;
    }

    @Basic(fetch = FetchType.LAZY)
    @Type(type = "org.hibernate.spatial.GeometryType")
    @Column(name = "geom", nullable = true)
    public Point getGeom() {
        return geom;
    }

    public void setGeom(Point geom) {
        this.geom = geom;
    }

    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "type", referencedColumnName = "`id`", nullable = false)
    public StopTypeEntity getStopTypeByType() {
        return stopTypeByType;
    }

    public void setStopTypeByType(StopTypeEntity stopTypeByType) {
        this.stopTypeByType = stopTypeByType;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "pk.start")
    @Fetch(FetchMode.SUBSELECT)
    @OrderBy("pk.start ASC")
    public Set<Connections> getStopsFrom() {
        return stopsFrom;
    }

    public void setStopsFrom(Set<Connections> stopsFrom) {
        this.stopsFrom = stopsFrom;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "pk.end")
    @Fetch(FetchMode.SUBSELECT)
    @OrderBy("pk.start ASC")
    public Set<Connections> getStopsTo() {
        return stopsTo;
    }

    public void setStopsTo(Set<Connections> stopsTo) {
        this.stopsTo = stopsTo;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "pk.start")
    @Fetch(FetchMode.SUBSELECT)
    @OrderBy("pk.start ASC")
    public Set<ExtConnections> getExtStopsFrom() {
        return extStopsFrom;
    }

    public void setExtStopsFrom(Set<ExtConnections> extStopsFrom) {
        this.extStopsFrom = extStopsFrom;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "pk.end")
    @Fetch(FetchMode.SUBSELECT)
    @OrderBy("pk.start ASC")
    public Set<ExtConnections> getExtStopsTO() {
        return extStopsTO;
    }

    public void setExtStopsTO(Set<ExtConnections> extStopsTO) {
        this.extStopsTO = extStopsTO;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "busStop")
    public List<TimetableEntity> getTimes() {
        return times;
    }

    public void setTimes(List<TimetableEntity> times) {
        this.times = times;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BusStopsEntity that = (BusStopsEntity) o;

        if (communityId != that.communityId) return false;
        if (gid != that.gid) return false;
        if (geom != null ? !geom.equals(that.geom) : that.geom != null) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = gid;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + communityId;
        result = 31 * result + (geom != null ? geom.hashCode() : 0);
        return result;
    }
}
