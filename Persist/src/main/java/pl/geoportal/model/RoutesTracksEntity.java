package pl.geoportal.model;

import javax.persistence.*;


@Entity
@Table(name = "routes_tracks", schema = "geoportal", catalog = "kzkgop")
public class RoutesTracksEntity {
    private Integer id;
    private Integer orderNumber;
    private Integer type;
    private BusStopsEntity busStop;
    private RoutesEntity route;

    public RoutesTracksEntity(){}

    public RoutesTracksEntity(Integer orderNumber, Integer type, BusStopsEntity busStop, RoutesEntity route) {
        this.orderNumber = orderNumber;
        this.type = type;
        this.busStop = busStop;
        this.route = route;
    }

    @Id
    @SequenceGenerator(name="routes_tracks_sequence",
            sequenceName="routes_tracks_id_seq",
            allocationSize=1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE,
            generator="routes_tracks_sequence")
    @Column(name = "id")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "order_number")
    public Integer getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(Integer orderNumber) {
        this.orderNumber = orderNumber;
    }

    @Basic
    @Column(name = "type")
    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    @ManyToOne
    @JoinColumn(name = "bus_stop_id", referencedColumnName = "`gid`", nullable = false)
    public BusStopsEntity getBusStop() {
        return busStop;
    }

    public void setBusStop(BusStopsEntity busStop) {
        this.busStop = busStop;
    }

    @ManyToOne
    @JoinColumn(name = "route_id", referencedColumnName = "`route_id`", nullable = false)
    public RoutesEntity getRoute() {
        return route;
    }

    public void setRoute(RoutesEntity route) {
        this.route = route;
    }



    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RoutesTracksEntity that = (RoutesTracksEntity) o;

        if (busStop != null ? !busStop.equals(that.busStop) : that.busStop != null) return false;
        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (orderNumber != null ? !orderNumber.equals(that.orderNumber) : that.orderNumber != null) return false;
        if (route != null ? !route.equals(that.route) : that.route != null) return false;
        if (type != null ? !type.equals(that.type) : that.type != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (orderNumber != null ? orderNumber.hashCode() : 0);
        result = 31 * result + (type != null ? type.hashCode() : 0);
        result = 31 * result + (busStop != null ? busStop.hashCode() : 0);
        result = 31 * result + (route != null ? route.hashCode() : 0);
        return result;
    }
}
