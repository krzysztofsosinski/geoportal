package pl.geoportal.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(schema ="geoportal", name = "connections", catalog = "kzkgop")
@AssociationOverrides({
        @AssociationOverride(name = "pk.start",
        joinColumns = @JoinColumn(name = "start_point")),
        @AssociationOverride(name = "pk.end",
        joinColumns =  @JoinColumn(name = "end_point"))
})
public class Connections implements Serializable{

    private ConnectionsPK pk = new ConnectionsPK();
    private Double time;
    private Double distance;
    private Long timeMili;

    @EmbeddedId
    public ConnectionsPK getPk() {
        return pk;
    }

    public void setPk(ConnectionsPK pk) {
        this.pk = pk;
    }

    @Basic
    @Column(name = "travel_time", nullable = true)
    public Double getTime() {
        return time;
    }

    public void setTime(Double time) {
        this.time = time;
    }

    @Basic
    @Column(name = "distance", nullable = true)
    public Double getDistance() {
        return distance;
    }

    public void setDistance(Double distance) {
        this.distance = distance;
    }

    @Basic
    @Column(name = "time_mili", nullable = true)
    public Long getTimeMili() {
        return timeMili;
    }

    public void setTimeMili(Long timeMili) {
        this.timeMili = timeMili;
    }
}
