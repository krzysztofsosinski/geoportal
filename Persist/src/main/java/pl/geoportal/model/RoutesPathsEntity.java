package pl.geoportal.model;

import com.vividsolutions.jts.geom.LineString;
import org.hibernate.annotations.Type;

import javax.persistence.*;

@Entity
@Table(name = "routes_paths", schema = "geoportal", catalog = "kzkgop")
public class RoutesPathsEntity {
    private Integer gid;
    private String duration;
    private Float length;
    private Integer direction;
    private LineString path;
    private RoutesEntity route;

    public RoutesPathsEntity(){}

    public RoutesPathsEntity(String duration, Float length, Integer direction, LineString path, RoutesEntity route) {
        this.duration = duration;
        this.length = length;
        this.direction = direction;
        this.path = path;
        this.route = route;
    }

    @Id
    @SequenceGenerator(name="routes_paths_sequence",
            sequenceName="routes_paths_gid_seq",
            allocationSize=1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE,
            generator="routes_paths_sequence")
    @Column(name = "gid")
    public Integer getGid() {
        return gid;
    }

    public void setGid(Integer gid) {
        this.gid = gid;
    }

    @Basic
    @Column(name = "duration")
    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    @Basic
    @Column(name = "length")
    public Float getLength() {
        return length;
    }

    public void setLength(Float length) {
        this.length = length;
    }

    @Basic
    @Column(name = "direction")
    public Integer getDirection() {
        return direction;
    }

    public void setDirection(Integer direction) {
        this.direction = direction;
    }

    @Basic
    @Type(type = "org.hibernate.spatial.GeometryType")
    @Column(name = "path", nullable = true)
    public LineString getPath() {
        return path;
    }

    public void setPath(LineString path) {
        this.path = path;
    }

    @ManyToOne
    @JoinColumn(name = "route_id", referencedColumnName = "`route_id`", nullable = false)
    public RoutesEntity getRoute() {
        return route;
    }

    public void setRoute(RoutesEntity route) {
        this.route = route;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RoutesPathsEntity that = (RoutesPathsEntity) o;

        if (gid != null ? !gid.equals(that.gid) : that.gid != null) return false;
        if (duration != null ? !duration.equals(that.duration) : that.duration != null) return false;
        if (length != null ? !length.equals(that.length) : that.length != null) return false;
        if (direction != null ? !direction.equals(that.direction) : that.direction != null) return false;
        if (path != null ? !path.equals(that.path) : that.path != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = gid != null ? gid.hashCode() : 0;
        result = 31 * result + (duration != null ? duration.hashCode() : 0);
        result = 31 * result + (length != null ? length.hashCode() : 0);
        result = 31 * result + (direction != null ? direction.hashCode() : 0);
        result = 31 * result + (path != null ? path.hashCode() : 0);
        return result;
    }
}
