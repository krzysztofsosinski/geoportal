package pl.geoportal.model;

import javax.persistence.*;
import java.sql.Time;

@Entity
@Table(name = "stop_times", schema = "geoportal", catalog = "kzkgop")
public class StopTimesEntity {
    private Integer id;
    private Time arrivalTime;
    private Time departureTime;
    private Integer orderNumber;
    private Float distTraveled;
    private TripsEntity tripsByTripId;
    private BusStopsEntity busStopsEntity;

    public StopTimesEntity(){}

    public StopTimesEntity(Time arrivalTime, Time departureTime, Integer orderNumber, TripsEntity tripsByTripId, BusStopsEntity busStopsEntity) {
        this.arrivalTime = arrivalTime;
        this.departureTime = departureTime;
        this.orderNumber = orderNumber;
        this.tripsByTripId = tripsByTripId;
        this.busStopsEntity = busStopsEntity;
    }

    @Id
    @SequenceGenerator(name="stop_times_sequence",
            sequenceName="stop_times_id_seq",
            allocationSize=1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE,
            generator="stop_times_sequence")
    @Column(name = "id")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "arrival_time")
    public Time getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(Time arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    @Basic
    @Column(name = "departure_time")
    public Time getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(Time departureTime) {
        this.departureTime = departureTime;
    }

    @Basic
    @Column(name = "order_number")
    public Integer getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(Integer orderNumber) {
        this.orderNumber = orderNumber;
    }

    @Basic
    @Column(name = "dist_traveled")
    public Float getDistTraveled() {
        return distTraveled;
    }

    public void setDistTraveled(Float distTraveled) {
        this.distTraveled = distTraveled;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        StopTimesEntity that = (StopTimesEntity) o;

        if (arrivalTime != null ? !arrivalTime.equals(that.arrivalTime) : that.arrivalTime != null) return false;
        if (departureTime != null ? !departureTime.equals(that.departureTime) : that.departureTime != null)
            return false;
        if (distTraveled != null ? !distTraveled.equals(that.distTraveled) : that.distTraveled != null) return false;
        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (orderNumber != null ? !orderNumber.equals(that.orderNumber) : that.orderNumber != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (arrivalTime != null ? arrivalTime.hashCode() : 0);
        result = 31 * result + (departureTime != null ? departureTime.hashCode() : 0);
        result = 31 * result + (orderNumber != null ? orderNumber.hashCode() : 0);
        result = 31 * result + (distTraveled != null ? distTraveled.hashCode() : 0);
        return result;
    }

    @ManyToOne
    @JoinColumn(name = "trip_id", referencedColumnName = "`trip_id`", nullable = false)
    public TripsEntity getTripsByTripId() {
        return tripsByTripId;
    }

    public void setTripsByTripId(TripsEntity tripsByTripId) {
        this.tripsByTripId = tripsByTripId;
    }

    @ManyToOne
    @JoinColumn(name = "stop_id", referencedColumnName = "`gid`", nullable = false)
    public BusStopsEntity getBusStopsEntity() {
        return busStopsEntity;
    }

    public void setBusStopsEntity(BusStopsEntity busStopsEntity) {
        this.busStopsEntity = busStopsEntity;
    }
}
