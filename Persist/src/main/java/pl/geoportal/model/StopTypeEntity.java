package pl.geoportal.model;

import javax.persistence.*;

@Entity
@Table(name = "stop_type", schema = "geoportal", catalog = "kzkgop")
public class StopTypeEntity {
    private Integer id;
    private String name;

    @Id
//    @SequenceGenerator(name="stop_type_id_seq",
//            sequenceName="stop_type_id_seq",
//            allocationSize=1)
//    @GeneratedValue(strategy = GenerationType.SEQUENCE,
//            generator="stop_type_id_seq")
    @Column(name = "id")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

//    @OneToMany(fetch = FetchType.LAZY, mappedBy = "stopTypeByType")
//    public Collection<BusStopsEntity> getBusStopsesById() {
//        return busStopsesById;
//    }
//
//    public void setBusStopsesById(Collection<BusStopsEntity> busStopsesById) {
//        this.busStopsesById = busStopsesById;
//    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        StopTypeEntity that = (StopTypeEntity) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }
}
