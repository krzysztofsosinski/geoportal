package pl.geoportal.config.security;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import pl.geoportal.model.Role;
import pl.geoportal.model.UserEntity;

import java.util.ArrayList;
import java.util.Collection;

public class SecurityUser extends UserEntity implements UserDetails {

    public SecurityUser(UserEntity userEntity) {
        if (userEntity != null) {
            this.setId(userEntity.getId());
            this.setName(userEntity.getName());
            this.setEmail(userEntity.getEmail());
            this.setPassword(userEntity.getPassword());
            this.setRole(userEntity.getRole());
            this.setEnabled(userEntity.getEnabled());
        }
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        Collection<GrantedAuthority> authorities = new ArrayList<>();
        Role role = this.getRole();

        SimpleGrantedAuthority authority = new SimpleGrantedAuthority(role.name());
        authorities.add(authority);

        return authorities;
    }

    @Override
    public String getUsername() {
        return super.getEmail();
    }

    @Override
    public String getPassword() {
        return super.getPassword();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

}
