package pl.geoportal.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.geoportal.client.timetable.TimetableParser;
import pl.geoportal.dto.ResponseDTO;
import pl.geoportal.logic.SearchLogic;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

@RestController
public class SearchController {

    @Autowired
    SearchLogic searchLogic;

    @Autowired
    TimetableParser timetableParser;

    @RequestMapping(
            value = "/search/{start}/{end}/{startTime}",
            method = RequestMethod.GET
    )
    public
    @ResponseBody
    List<List<ResponseDTO>> fastestPath(@PathVariable Integer start, @PathVariable Integer end, @PathVariable String startTime) {
        try {
            return searchLogic.shortestPath(start, end, startTime);
        } catch (ParseException e) {
            e.printStackTrace();
            return new ArrayList<>();
        }
    }

//    @RequestMapping(
//            value = "/times",
//            method = RequestMethod.GET
//    )
//    public
//    @ResponseBody
//    String times() {
//        searchLogic.updateTimes();
//        return "OK";
//    }

    @RequestMapping(
            value = "/expanded-tracks",
            method = RequestMethod.GET
    )
    public
    @ResponseBody
    String expandTracks() {
        try {
            timetableParser.parseAll();
//            timetableParser.addAnnotation();
        } catch (IOException | ParseException e) {
            e.printStackTrace();
        }
        return "OK";
    }

    @RequestMapping(
            value = "/smaller-tracks",
            method = RequestMethod.GET
    )
    public
    @ResponseBody
    String simpleTracks() {
        timetableParser.createExtConnections();
        return "OK";
    }
}
