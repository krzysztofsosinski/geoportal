package pl.geoportal.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import pl.geoportal.client.*;
import pl.geoportal.db.BusStopService;
import pl.geoportal.db.RouteService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class BusStopsRest {

    @Autowired
    BusStopService busStopService;

    @Autowired
    RouteService routeService;

    @Autowired
    KzkGopClient kzkGopClient;

    @Autowired
    KzkGopParser kzkGopParser;

    @Autowired
    GraphBuilder graphBuilder;

    @RequestMapping(
            value = "/bus-stops",
            method = RequestMethod.GET
    )
    @ResponseStatus(HttpStatus.CREATED)
    public
    @ResponseBody
    String addBusStops(@RequestParam(value = "lat") double lat, @RequestParam(value = "lng") double lng) {
        try {
            BusStopsWrapper busStopsWrapper = kzkGopClient.parseBusStops(lat, lng);
            busStopsWrapper.getBusStopDTOs().forEach(busStopService::updateBusStop);
            return "SUCCESS";
        } catch (Exception ex) {
            return ex.getMessage();
        }
    }

    @RequestMapping(
            value = "/search-bus-stops",
            method = RequestMethod.GET
    )
    @ResponseStatus(HttpStatus.CREATED)
    public
    @ResponseBody
    SearchedBusStops getBusStops(@RequestParam(value = "search") String searchNeedle, @RequestParam(value = "max") int max) {
        try {
            List<BusStopDTO> stops = busStopService.getBusNames(searchNeedle, max);
            SearchedBusStops searchedBusStops = new SearchedBusStops();
            searchedBusStops.items = stops;
            return searchedBusStops;
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    @RequestMapping(
            value = "/search-bus-lines",
            method = RequestMethod.GET
    )
    @ResponseStatus(HttpStatus.CREATED)
    public
    @ResponseBody
    SearchedBusLines getBusLines(@RequestParam(value = "search") String searchNeedle, @RequestParam(value = "max") int max) {
        try {
            List<RouteDTO> routes = routeService.searchRoutes(searchNeedle, max);
            return new SearchedBusLines(routes);
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    @RequestMapping(
            value = "/update-timetable",
            method = RequestMethod.GET
    )
    public
    @ResponseBody
    String parseKzkGopTimetable() {
        kzkGopParser.parseTransportLines();
        return "success";
    }

    @RequestMapping(
            value = "/graph",
            method = RequestMethod.GET
    )
    public
    @ResponseBody
    String createGraph() {
        graphBuilder.buildConnections();
        return "success";
    }

//    @RequestMapping(
//            value = "/graph-null",
//            method = RequestMethod.GET
//    )
//    public
//    @ResponseBody
//    String nullDeleter() {
//        try {
//            graphBuilder.updateGeoms();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        return "success";
//    }

    @RequestMapping(
            value = "/traversing",
            method = RequestMethod.GET
    )
    public
    @ResponseBody
    String traversingGraph() {
        graphBuilder.traversingGraph();
        return "success";
    }

    @RequestMapping(
            value = "/routes-lines",
            method = RequestMethod.GET
    )
    public
    @ResponseBody
    String getRoutesTracks() {
        graphBuilder.addTracksToRoutes();
        return "success";
    }

    @RequestMapping(
            value = "/bus-stops-all",
            method = RequestMethod.GET
    )
    @ResponseStatus(HttpStatus.CREATED)
    public
    @ResponseBody
    String batchRun() {
        try {
            Map<Double, Double> coords = new HashMap<>();
            coords.put(50.1425360, 19.1310540);
            coords.put(50.4271180, 19.3948370);
            coords.put(50.5018740, 18.9394890);
            coords.put(50.4446470, 19.1284380);
            coords.put(50.1790180, 18.9038040);
            coords.put(50.2080470, 19.1660510);
            coords.put(50.5706840, 19.3144640);
            coords.put(50.1833330, 18.7666670);
            coords.put(50.1437780, 18.7756860);
            coords.put(50.4614330, 19.0428330);
            coords.put(50.3789110, 18.9270480);
            coords.put(50.2146740, 18.5625630);
            coords.put(50.4880300, 19.3389390);
            coords.put(50.3775, 19.0961);
            coords.put(50.3955790, 18.6346380);
            coords.put(50.3998140, 18.9017630);
            coords.put(50.2558290, 18.8555700);
            coords.put(50.3533090, 18.4096160);
            coords.put(50.1021740, 18.5462850);
            coords.put(50.3264310, 19.0295710);
            coords.put(50.4665750, 19.2302200);
            coords.put(50.3045980, 19.3880200);
            coords.put(50.2862640, 19.1040790);
            coords.put(50.2718640, 18.5286270);
            coords.put(50.4359090, 18.9462070);
            coords.put(50.2964330, 18.9171300);
            coords.put(50.4359470, 18.8460250);
            coords.put(50.4542100, 18.5224540);
            coords.put(50.5299550, 18.7160350);
            coords.put(50.1218010, 19.0200020);
            coords.put(50.5096410, 18.6161980);
            coords.put(50.3647680, 19.0367910);
            coords.put(50.3249280, 18.7857190);
            coords.put(50.4166670, 18.7500000);

            for (Map.Entry<Double, Double> entry : coords.entrySet()) {
                BusStopsWrapper busStopsWrapper = kzkGopClient.parseBusStops(entry.getKey(), entry.getValue());
                busStopsWrapper.getBusStopDTOs().forEach(busStopService::saveBusStop);
            }

            return "SUCCESS";
        } catch (Exception ex) {
            return ex.getMessage();
        }
    }
}
