<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<%--<header class="main-header">--%>
    <%--<h3>--%>
        <%--<a href="<c:url value="/"/>"--%>
           <%--title='<spring:message code="project.name"/>'--%>
                <%--><i class="fa fa-home"></i>--%>
            <%--<spring:message code="project.name"/>--%>
    <%--</a>--%>
    <%--</h3>--%>
<%--</header>--%>
<%--<div id="loginForm">--%>
    <%--<a class="icon entypo-user" href="<c:url value='/logout' />" title='<spring:message code="header.logout"/>'>--%>
        <%--<spring:message code="header.logout"/>&nbsp;(${userEntity.name})--%>
    <%--</a>--%>
<%--</div>--%>

<div class="header-container">
    <header class="wrapper clearfix">
        <h3>
            <a href="<c:url value="/"/>"
               title='<spring:message code="project.name"/>'
                    ><i class="fa fa-home"></i>
                <spring:message code="project.name"/>
            </a>
        </h3>
        <div id="loginForm">
            <a class="icon entypo-user" href="<c:url value='/logout' />" title='<spring:message code="header.logout"/>'>
                <spring:message code="header.logout"/>&nbsp;(${userEntity.name})
            </a>
        </div>
        <%--<nav>--%>
            <%--<ul>--%>
                <%--<li><a href="#">nav ul li a</a></li>--%>
                <%--<li><a href="#">nav ul li a</a></li>--%>
                <%--<li><a href="#">nav ul li a</a></li>--%>
            <%--</ul>--%>
        <%--</nav>--%>
    </header>
</div>

