<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<!doctype html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="pl"> <![endif]-->
<!--[if IE 7]> <html class="no-js lt-ie9 lt-ie8" lang="pl"> <![endif]-->
<!--[if IE 8]> <html class="no-js lt-ie9" lang="pl"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="pl"> <!--<![endif]-->
<head>
    <link rel="shortcut icon" type="image/x-icon" href="${pageContext.request.contextPath}/favicon.ico" />
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Informacja turystyczna KZK GOP</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="apple-touch-icon" href="apple-touch-icon.png">

    <title><spring:message code="project.title"/></title>

    <link href="<c:url value='/resources/css/normalize.min.css'  />" rel="stylesheet"/>
    <link href="<c:url value='/resources/css/ui.min.css'  />" rel="stylesheet"/>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/ol3/3.5.0/ol.min.css" rel="stylesheet"/>
    <link href="<c:url value='/resources/css/geoportal.css'  />" rel="stylesheet"/>

    <script data-main="/resources/js/main" src="<c:url value='/resources/js/require.js' />"></script>

</head>
<body>
<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade
    your browser</a> to improve your experience.</p>
<![endif]-->

<%--<tiles:insertAttribute name="header"/>--%>

<tiles:insertAttribute name="body"/>

<%--<tiles:insertAttribute name="footer"/>--%>

<script>
    (function (b, o, i, l, e, r) {
        b.GoogleAnalyticsObject = l;
        b[l] || (b[l] =
                function () {
                    (b[l].q = b[l].q || []).push(arguments)
                });
        b[l].l = +new Date;
        e = o.createElement(i);
        r = o.getElementsByTagName(i)[0];
        e.src = '//www.google-analytics.com/analytics.js';
        r.parentNode.insertBefore(e, r)
    }(window, document, 'script', 'ga'));
    ga('create', 'UA-XXXXX-X', 'auto');
    ga('send', 'pageview');
</script>
</body>
</html>