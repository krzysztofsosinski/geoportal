<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<div class="login-container">
    <div class="login">
        <h1><spring:message code='project.name'/></h1>
        <div class="alert alert-error" ng-class="{'': displayLoginError == true, 'none': displayLoginError == false}">
            <spring:message code="login.error" />
        </div>
        <form method="post" action="<c:url value="/login"></c:url>">
            <input name="username" id="username" type="text"
                   placeholder="<spring:message code='sample.email' /> "><br/>
            <input name="password" id="password" type="password" placeholder="Password"><br/>
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>

            <p class="remember_me">
                <label>
                    <label>
                        <input type="checkbox" name="remember_me" id="remember_me">
                        Remember me on this computer
                    </label>
                </label>
            </p>

            <p class="submit">
                <button type="submit" name="submit" class="btn btn-inverse btn-block"><spring:message
                        code="login.signIn"/></button>
            </p>
        </form>
    </div>

    <div class="login-help">
        <p>Forgot your password? <a href="#">Click here to reset it</a>.</p>
    </div>
</div>

<script src="<c:url value='/resources/js/pages/login.js' />"></script>

