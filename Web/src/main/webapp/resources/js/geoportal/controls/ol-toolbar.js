define([
    'ol3',
    'jquery',
    'text!templates/ol-controls/search_form_stop.html',
    'text!templates/ol-controls/search_form_line.html',
    'w2ui/w2layout',
    'w2ui/w2toolbar',
    'w2ui/w2sidebar',
    'w2ui/w2popup'
], function(ol, $, search_bus, search_line) {

    ol.control.MapToolbar = function (opt_options) {

        var pstyle = 'border: 1px solid #dfdfdf; padding: 5px;';
        var options = opt_options || {};
        var element = document.createElement('div');
        element.className = 'ol-map-toolbar';
        $('#map-container').append(element);
        $(element).css({
            'width': '500px', 'height': '140px'
        });

        $(element).w2layout({
            name: 'map-search-toolbar',
            width: '100%',
            panels: [
                { type: 'top', size: 70, resizable: false, style: pstyle, content: search_bus},
                { type: 'main', size: 70, resizable: false, style: pstyle, content: search_line}
            ]
        });

        $(element).find("#search-bus-form").w2form({
            name  : 'form-search-bus',
            url   : 'server/post',
            fields: [
                { field: 'bus_stop', type: 'list', required: true, options: {
                    url: '/search-bus-stops',
                    cacheMax: 100,
                    renderItem: function (item) {
                        return item.text;
                    },
                    renderDrop: function (item) {
                        return item.text;
                    },
                    compare: function (item, search) {
                        var name = search;
                        var match = false;
                        var re1 = new RegExp(name, 'i');
                        if (re1.test(item.text)) match = true;
                        return match;
                    } }}
            ],
            actions: {
                save: function () {
                    this.save();
                }
            }
        });

        $(element).find("#search-line-form").w2form({
            name  : 'form-search-line',
            url   : 'server/post',
            fields: [
                { field: 'bus_lines', type: 'list', required: true, options: {
                    url: '/search-bus-lines',
                    cacheMax: 100,
                    renderItem: function (item) {
                        return item.text;
                    },
                    renderDrop: function (item) {
                        return item.text;
                    },
                    compare: function (item, search) {
                        var name = search;
                        var match = false;
                        var re1 = new RegExp(name, 'i');
                        if (re1.test(item.text)) match = true;
                        return match;
                    } }}
            ],
            actions: {
                save: function () {
                    w2popup.open({
                        title   : 'Popup',
                        width   : 900,
                        height  : 600,
                        showMax : true,
                        body    : '<div id="main" style="position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px;"></div>',
                        onOpen  : function (event) {
                            event.onComplete = function () {
                                $('#w2ui-popup #main').w2render('layout');
                                w2ui.layout.content('left', w2ui.sidebar1);
                                w2ui.layout.content('right', w2ui.sidebar2);
                                w2ui.layout.content('main', "<b> dzialam </b>");
                            }
                        },
                        onToggle: function (event) {
                            event.onComplete = function () {
                                w2ui.layout.resize();
                            }
                        }
                    });
                    //this.save();
                }
            }
        });

        // widget configuration
        var config = {
            layout: {
                name: 'layout',
                padding: 0,
                panels: [
                    { type: 'top', size: 32, content: '<div style="padding: 7px;">Rozk�ad jazdy</div>', style: 'border-bottom: 1px solid silver;' },
                    { type: 'left', size: 200, resizable: true, minSize: 120 },
                    { type: 'right', size: 200, resizable: true, minSize: 120 },
                    { type: 'main', minSize: 350, overflow: 'hidden' }
                ]
            },
            sidebar1: {
                name: 'sidebar1',
                nodes: [
                    { id: 'general', text: 'Przystanki', group: true, expanded: true, nodes: [
                        { id: 'grid', text: 'Grid', img: 'icon-page', selected: true },
                        { id: 'html', text: 'Some HTML', img: 'icon-page' }
                    ]}
                ],
                onClick: function (event) {
                    switch (event.target) {
                        case 'grid':
                            w2ui.layout.content('main', '<div style="padding: 10px">Some HTML</div>');
                            $(w2ui.layout.el('main'))
                                .removeClass('w2ui-grid')
                                .css({
                                    'border-left': '1px solid silver'
                                });
                            break;
                        case 'html':
                            w2ui.layout.content('main', '<div style="padding: 10px">Some HTML</div>');
                            $(w2ui.layout.el('main'))
                                .removeClass('w2ui-grid')
                                .css({
                                    'border-left': '1px solid silver'
                                });
                            break;
                    }
                }
            },
            sidebar2: {
                name: 'sidebar2',
                nodes: [
                    { id: 'general', text: 'Przystanki', group: true, expanded: true, nodes: [
                        { id: 'grid', text: 'Grid', img: 'icon-page', selected: true },
                        { id: 'html', text: 'Some HTML', img: 'icon-page' }
                    ]}
                ],
                onClick: function (event) {
                    switch (event.target) {
                        case 'grid':
                            w2ui.layout.content('main', '<div style="padding: 10px">Some HTML</div>');
                            $(w2ui.layout.el('main'))
                                .removeClass('w2ui-grid')
                                .css({
                                    'border-left': '1px solid silver'
                                });
                            break;
                        case 'html':
                            w2ui.layout.content('main', '<div style="padding: 10px">Some HTML</div>');
                            $(w2ui.layout.el('main'))
                                .removeClass('w2ui-grid')
                                .css({
                                    'border-left': '1px solid silver'
                                });
                            break;
                    }
                }
            }
        };

        $(function () {
            // initialization in memory
            $().w2layout(config.layout);
            $().w2sidebar(config.sidebar1);
            $().w2sidebar(config.sidebar2);
        });


        ol.control.Control.call(this, {
            element: element,
            target: options.target
        });

    };

    ol.inherits(ol.control.MapToolbar, ol.control.Control);

    return ol.control.MapToolbar;

});