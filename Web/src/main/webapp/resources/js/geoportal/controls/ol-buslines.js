define([
    'ol3',
    'jquery',
    'text!templates/ol-controls/search_tracks_form.html',
    'w2ui/w2layout',
    'w2ui/w2form'
], function(ol, $, search_form) {

    ol.control.BusLines = function (opt_options) {

        var options = opt_options || {};
        var element = $(opt_options.id);

        element.append(search_form);

        element.find('#search-tracks-form').w2form({
            name  : 'form-search-tracks',
            url   : 'server/post',
            fields: [
                { field: 'start_point', type: 'list', required: true, options: {
                    url: '/search-bus-stops',
                    cacheMax: 100,
                    renderItem: function (item) {
                        return item.text;
                    },
                    renderDrop: function (item) {
                        return item.text;
                    },
                    compare: function (item, search) {
                        var name = search;
                        var match = false;
                        var re1 = new RegExp(name, 'i');
                        if (re1.test(item.text)) match = true;
                        return match;
                    } }},
                { field: 'end_point',  type: 'list', required: true, options: {
                    url: '/search-bus-stops',
                    cacheMax: 100,
                    renderItem: function (item) {
                        console.log(item);
                        return item.text;
                    },
                    renderDrop: function (item) {
                        return item.text;
                    },
                    compare: function (item, search) {
                        var name = search;
                        var match = false;
                        var re1 = new RegExp(name, 'i');
                        if (re1.test(item.text)) match = true;
                        return match;
                    } }},
                { field: 'start_time',   type: 'datetime'}
            ],
            actions: {
                reset: function () {
                    this.clear();
                },
                save: function () {
                    this.save();
                }
            }
        });

        element.addClass('ol-transport-manager');

        ol.control.Control.call(this, {
            element: element[0],
            target: options.target
        });

    };

    ol.inherits(ol.control.BusLines, ol.control.Control);
    return ol.control.BusLines;

});
