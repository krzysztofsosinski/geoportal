define([
    'ol3',
    'jquery',
    'text!templates/ol-controls/login_form.html',
    'w2ui/w2toolbar'
], function(ol, $, login_form) {

    ol.control.Account = function (opt_options) {

        var options = opt_options || {};
        var element = document.createElement('div');
        element.className = 'ol-account';
        $(element).css({
            'padding': '4px', 'border': '1px solid silver', 'border-radius': '3px'
        });

        $(element).w2toolbar({
            name: 'account-toolbar',
            items: [
                { type: 'html',  id: 'item1',
                    html: login_form
                },
                { type: 'button',  id: 'item2',  caption: 'Zaloguj', icon: 'fa sign-in', hint: 'Zaloguj do portalu' }
            ],
            onClick: function (event) {
                console.log('Target: '+ event.target, event);
            }
        });

        $('#map-container').append(element);
        ol.control.Control.call(this, {
            element: element,
            target: options.target
        });

    };

    ol.inherits(ol.control.Account, ol.control.Control);

    return ol.control.Account;

});