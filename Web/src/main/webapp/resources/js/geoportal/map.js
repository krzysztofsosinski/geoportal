define([
    "jquery",
    "underscore",
    "ol3",
    'map_controls/ol-layerswitcher',
    'map_controls/ol-buslines',
    'map_controls/ol-toolbar',
    'map_controls/ol-account'
], function ($, _, ol3, LS, BL, TB, AC) {

    var Map = function () {
        var this_ = this;

        this.view = new ol3.View({
            center: ol3.proj.transform([19.031, 50.2561], 'EPSG:4326', 'EPSG:3857'),
            zoom: 13,
            maxZoom: 23,
            projection: 'EPSG:3857'
        });

        var overlayGroup = new ol3.layer.Group({
            title: 'Overlays',
            layers: []
        });

        var webglAvailable = function() {
            try {
                var canvas = document.createElement("canvas");
                return !!
                        window.WebGLRenderingContext &&
                    (canvas.getContext("webgl") ||
                    canvas.getContext("experimental-webgl"));
            } catch(e) {
                return false;
            }
        };

        var renderer = webglAvailable() ? 'webgl' : 'canvas';
        console.log(renderer);
        var controls = [
            new ol3.control.Attribution(),
            //new ol3.control.MousePosition({
            //    undefinedHTML: 'outside',
            //    projection: 'EPSG:4326',
            //    coordinateFormat: function(coordinate) {
            //        return ol3.coordinate.format(coordinate, '{x}, {y}', 4);
            //    }
            //}),
            new ol3.control.OverviewMap({
                collapsed: false
            })
        ];

        this.map = new ol3.Map({
            layers: [
                new ol3.layer.Group({
                    'title': 'Base maps',
                    renderer : renderer,
                    layers: [
                        new ol3.layer.Tile({
                            title: 'Road',
                            type: 'base',
                            visible: false,
                            style: 'Road',
                            source: new ol3.source.MapQuest({layer: 'osm'})
                        }),
                        new ol3.layer.Tile({
                            title: 'Bing',
                            name: 'Road',
                            type: 'base',
                            visible: false,
                            preload: Infinity,
                            source: new ol3.source.BingMaps({
                                key: 'Akj_uDpXSC2OddkCggkA5sD2a9-qPYIS5bFkSjR5hIb6MVFw5J0gIqU7lLBX68Gn',
                                imagerySet: 'Road',
                                minResolution: 200,
                                maxResolution: 2000
                            })
                        }),
                        new ol3.layer.Tile({
                            title: 'OSM',
                            visible: true,
                            type: 'base',
                            source: new ol3.source.OSM()
                        })
                    ]
                }),
                overlayGroup
            ],
            controls: controls,
            target: 'map-container',
            view: this_.view
        });

        var layerSwitcher = new LS();
        var busLines = new BL({id: '#bus-lines'});
        var toolbar = new TB();
        //var account = new AC();

        this.map.addControl(layerSwitcher);
        this.map.addControl(busLines);
        this.map.addControl(toolbar);
        //this.map.addControl(account);


        overlayGroup.getLayers().push(new ol3.layer.Tile({
            title: 'Bus stops',
            visible: true,
            source: new ol3.source.TileWMS({
                url: 'http://localhost:8888/geoserver/kzkgop/wms',
                params: {
                    'FORMAT': 'image/png',
                    'TILED': true,
                    'STYLES': '',
                    'TRANSPARENT': 'true',
                    'LAYERS': 'kzkgop:bus_stops',
                    'FORMAT_OPTIONS': 'antialiasing:on'
                }
            })
        }));

        overlayGroup.getLayers().push(new ol3.layer.Tile({
                title: 'Transport routes',
                visible: false,
                source: new ol3.source.TileWMS({
                    url: 'http://localhost:8888/geoserver/kzkgop/wms',
                    params: {
                        'FORMAT': 'image/png',
                        'TILED': true,
                        'STYLES': '',
                        'TRANSPARENT': 'true',
                        'LAYERS': 'kzkgop:routes_tracks',
                        'FORMAT_OPTIONS': 'antialiasing:on'
                    }
                })
            })
        );

        overlayGroup.getLayers().push(new ol3.layer.Tile({
                title: 'Transport paths',
                visible: true,
                source: new ol3.source.TileWMS({
                    url: 'http://localhost:8888/geoserver/kzkgop/wms',
                    params: {
                        'FORMAT': 'image/png',
                        'TILED': true,
                        'STYLES': '',
                        'TRANSPARENT': 'true',
                        'LAYERS': 'kzkgop:routes_paths',
                        'FORMAT_OPTIONS': 'antialiasing:on'
                    }
                })
            })
        );

        this.init = function () {
            return this.map;
        };

        this.map.updateSize();
    };


    return {
        m: new Map()
    }

});
