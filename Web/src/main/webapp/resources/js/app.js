// Filename: app.js
define([
    'jquery',
    'underscore',
    'backbone',
    'router',
    'map',
    'w2ui/w2popup'
], function($, _, Backbone, Router, Map){
    var initialize = function(){
        // Pass in our Router module and call it's initialize function
        Router.initialize();

    };

    return {
        initialize: initialize
    };
});
