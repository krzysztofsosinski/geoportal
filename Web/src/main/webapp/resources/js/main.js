// Filename: main.js

// Require.js allows us to configure shortcut alias
// There usage will become more apparent further along in the tutorial.
requirejs.config({
    urlArgs : "dev=" + Math.random(),
    paths: {
        modernizr: 'libs/modernizr-respond.min',
        jquery: 'libs/jquery.min',
        underscore: 'libs/underscore-min',
        backbone: 'libs/backbone-min',
        ol3: ['https://cdnjs.cloudflare.com/ajax/libs/ol3/3.5.0/ol.min','libs/ol'],
        map: 'geoportal/map',
        text: 'libs//text',
        map_controls : 'geoportal/controls'
    },

    packages: [
        {
            name: 'w2ui',
            location: 'libs/ui'
        }
    ],

    shim: {
        'w2ui/w2utils': {
            deps: ['jquery'],
            exports: 'w2ui',
            init: function() {
                return {
                    'w2ui': w2ui,
                    'w2obj': w2obj,
                    'w2utils': w2utils
                } ;
            }
        },
        'w2ui/w2layout'  : [ 'w2ui/w2utils', 'w2ui/w2toolbar', 'w2ui/w2tabs'],
        'w2ui/w2grid'    : [ 'w2ui/w2utils', 'w2ui/w2toolbar', 'w2ui/w2fields'],
        'w2ui/w2toolbar' : [ 'w2ui/w2utils'],
        'w2ui/w2sidebar' : [ 'w2ui/w2utils'],
        'w2ui/w2tabs'    : [ 'w2ui/w2utils'],
        'w2ui/w2form'    : [ 'w2ui/w2utils', 'w2ui/w2fields', 'w2ui/w2tabs', 'w2ui/w2toolbar' ],
        'w2ui/w2popup'   : [ 'w2ui/w2utils'],
        'w2ui/w2fields'  : [ 'w2ui/w2utils']
    }

});

require([
    // Load our app module and pass it to our definition function
    'app'
], function(App){
    // The "app" dependency is passed in as "App"
    App.initialize();
});


//require([
//    'map'
//], function(Map){
//
//});
