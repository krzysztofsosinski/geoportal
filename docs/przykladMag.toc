\contentsline {section}{Wst\k ep}{7}
\contentsline {paragraph}{Opis zawarto\'sci przedstawionej pracy}{7}
\contentsline {section}{\numberline {1}\kernsekcyjny Algorytmy wyszukiwania wielokryterialnego}{9}
\contentsline {subsection}{\numberline {1.1}\kernsubsekcyjny Przedstawienie problemu}{9}
\contentsline {subsection}{\numberline {1.2}\kernsubsekcyjny Za\IeC {\l }o\.zenia poszukiwanego rozwi\k azania}{10}
\contentsline {subsection}{\numberline {1.3}\kernsubsekcyjny Opis algorytm\'ow}{12}
\contentsline {subsubsection}{\numberline {1.3.1}\kernsubsubsekcyjny Algorytm oznaczania/etykietowania (ang. Label Algorithm)}{14}
\contentsline {paragraph}{Przypadek jednokryterialny}{14}
\contentsline {paragraph}{Struktury danych dla zbioru oznacze\'n $X$}{15}
\contentsline {paragraph}{Przypadek wielokryterialny}{18}
\contentsline {subsubsection}{\numberline {1.3.2}\kernsubsubsekcyjny Algorytm K-tej najkr\'otszej \'scie\.zki}{19}
\contentsline {paragraph}{Konstruowanie drzewa \'scie\.zek}{22}
\contentsline {section}{\numberline {2}\kernsekcyjny Tytu\IeC {\l } drugiego rozdzia\IeC {\l }u. \relax Bardzo d\IeC {\l }ugi tytu\IeC {\l }. \relax Jego \\formatowanie jest trudniejsze}{23}
\contentsline {section}{Literatura}{25}
