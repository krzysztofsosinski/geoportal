-- Create a join table to collect tags for entries.
CREATE TABLE bus_stops_types (
  bus_stops_gid  INTEGER REFERENCES geoportal.bus_stops(gid)
                    ON UPDATE CASCADE
                    ON DELETE CASCADE,
  type_id    INTEGER REFERENCES geoportal.stop_type(id)
                    ON UPDATE CASCADE

  CONSTRAINT bus_stops_types_pkey PRIMARY KEY (bus_stops_gid, type_id)
);