package pl.geoportal.client;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class RouteDTO {

    public Integer id;
    public String text;

}
