package pl.geoportal.client;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.ArrayList;
import java.util.List;


@JsonIgnoreProperties(ignoreUnknown = true)
public class SearchedBusLines {
    public String status = "success";
    public List<RouteDTO> items = new ArrayList<>();

    public SearchedBusLines(List<RouteDTO> items) {
        this.items = items;
    }
}
