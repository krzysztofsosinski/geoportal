package pl.geoportal.client;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.ArrayList;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SearchedBusStops {

    public String status = "success";
    public List<BusStopDTO> items = new ArrayList<>();

}
