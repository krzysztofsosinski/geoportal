package pl.geoportal.client;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

@Service
public class KzkGopClient {

    public BusStopsWrapper parseBusStops(double lat, double lng) throws IOException {

        URL kzkGopUrl = new URL("http://rozklady.kzkgop.pl/ajax/mapa/getMarkers.php?lat=" + lat + "&lng=" + lng);
        URLConnection kzkGop = kzkGopUrl.openConnection();
        BufferedReader in = new BufferedReader(
                new InputStreamReader(
                        kzkGop.getInputStream(), "UTF-8"));

        ObjectMapper mapper = new ObjectMapper();
        BusStopsWrapper busStopsWrapper = mapper.readValue(in, BusStopsWrapper.class);
        in.close();
        return busStopsWrapper;
    }

}
