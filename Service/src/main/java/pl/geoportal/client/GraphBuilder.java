package pl.geoportal.client;

import com.vividsolutions.jts.geom.*;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;
import pl.geoportal.client.RoutingXml.RoutingDTO;
import pl.geoportal.dto.Osm2PoJsonDTO;
import pl.geoportal.dto.OsmFeaturesDTO;
import pl.geoportal.model.BusStopsEntity;
import pl.geoportal.model.RoutesEntity;
import pl.geoportal.model.RoutesPathsEntity;
import pl.geoportal.model.RoutesTracksEntity;
import pl.geoportal.persistence.BusStopsDao;
import pl.geoportal.persistence.GenericDao;
import pl.geoportal.persistence.RoutesTracksDao;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.io.*;
import java.nio.charset.Charset;
import java.util.*;
import java.util.stream.Collectors;

@Component
public class GraphBuilder {

    public final static double AVERAGE_RADIUS_OF_EARTH = 6371;

    @Autowired
    GenericDao<RoutesEntity, Integer> genericDao;

    @Autowired
    BusStopsDao busStopsDao;

    @Autowired
    RoutesTracksDao routesTracksDao;

    @Transactional
    @SuppressWarnings("unchecked")
    public void buildConnections() {
        genericDao.senEntityClass(RoutesEntity.class);
        List<RoutesEntity> routes = genericDao.getList();
        HashSet<Edge> connections = new LinkedHashSet<>();
        HashSet<String> names = new HashSet<>();
        HashSet<RoutesTracksEntity> toDelete = new HashSet<>();
        for (RoutesEntity routesEntity : routes) {
            List<RoutesTracksEntity> routesTracks = routesTracksDao.getByRoute(routesEntity);
            List<RoutesTracksEntity> filtered = routesTracks.stream()
                    .filter(p -> p.getType() == 1)
                    .sorted((s1, s2) -> s1.getOrderNumber().compareTo(s2.getOrderNumber()))
                    .collect(Collectors.toList());
            for (int i = 0; i < filtered.size() - 1; ++i) {
                if (Objects.equals(filtered.get(i).getBusStop().getGid(), filtered.get(i + 1).getBusStop().getGid())) {
                    names.add(filtered.get(i).getRoute().getRouteId().toString());
                    toDelete.add(filtered.get(i + 1));
                }
                connections.add(new Edge(
                                filtered.get(i).getBusStop().getGid(),
                                filtered.get(i + 1).getBusStop().getGid(),
                                filtered.get(i).getType()
                        )
                );
            }
            filtered = routesTracks.stream()
                    .filter(p -> p.getType() == 2)
                    .sorted((s1, s2) -> s1.getOrderNumber().compareTo(s2.getOrderNumber()))
                    .collect(Collectors.toList());

            for (int i = 0; i < filtered.size() - 1; ++i) {
                if (Objects.equals(filtered.get(i).getBusStop().getGid(), filtered.get(i + 1).getBusStop().getGid())) {
                    names.add(filtered.get(i).getRoute().getRouteId().toString());
                    toDelete.add(filtered.get(i + 1));
                }
                connections.add(new Edge(
                                filtered.get(i).getBusStop().getGid(),
                                filtered.get(i + 1).getBusStop().getGid(),
                                filtered.get(i).getType()
                        )
                );
            }

        }

//        toDelete.forEach(genericDao::deleteN);

        Map<String, Object> params = new HashMap<>();
        final String sql = "INSERT INTO geoportal.connections (start_point,end_point,direction) VALUES (:a, :b, :direction) ";
        for (Edge edge : connections) {
            if (Objects.equals(edge.getStartPoint(), edge.getEndPoint())) continue;
            params.put("a", edge.getStartPoint());
            params.put("b", edge.getEndPoint());
            params.put("direction", edge.getDirection());
            genericDao.runNativeQuery(sql, params);
        }


//        RestTemplate restTemplate = new RestTemplate();
//        String result = "";
//        RoutingDTO routingDTO = new RoutingDTO();
//        try {
//            ResponseEntity<String> response = restTemplate.getForEntity(
//                    "http://openls.geog.uni-heidelberg.de/testing2015/route?Start=8.6817521,49.4173462&End=8.6828883,49.4067577&Via=&lang=de&distunit=KM&routepref=Fastest&avoidAreas=&useTMC=false&noMotorways=false&noTollways=false&instructions=false",
//                    String.class);
//            result = response.getBody().replaceFirst("<xls:XLS.+xsd\">", "<xls:XLS>");
//            result = result.replaceAll("xls:|gml:|xsi:", "");
//            JAXBContext jaxbContext = JAXBContext.newInstance(RoutingDTO.class);
//            Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
//            routingDTO = (RoutingDTO) unmarshaller.unmarshal(new StringReader(result));
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        }
//        List<String> bla = routingDTO.routingResponse.routingResponseSubclass.routeGeometry.gmlLineString.pos;
        String asd = "";
    }

    @Transactional
    @SuppressWarnings("unchecked")
    public void addTracksToRoutes() {
        genericDao.senEntityClass(RoutesEntity.class);
        List<RoutesEntity> routes = genericDao.getList();
        List<RoutesPathsEntity> routesPaths = new ArrayList<>();
        int i = 0;
        final GeometryFactory factory = new GeometryFactory(new PrecisionModel(), 4326);
        for (RoutesEntity routesEntity : routes) {
            if (i == 10) {
                break;
            }
            List<RoutesTracksEntity> routesTracks = routesTracksDao.getByRoute(routesEntity);
            List<RoutesTracksEntity> filtered = routesTracks.stream()
                    .filter(p -> p.getType() == 2)
                    .sorted((s1, s2) -> s1.getOrderNumber().compareTo(s2.getOrderNumber()))
                    .collect(Collectors.toList());
            if (filtered.size() == 0) continue;
            RestTemplate restTemplate = new RestTemplate();
            String result;
            Optional<RoutingDTO> routingDTO = Optional.empty();
            List<Coordinate> coordinates = new LinkedList<>();
            try {
                ResponseEntity<String> response = restTemplate.getForEntity(
                        "http://openls.geog.uni-heidelberg.de/testing2015/route?" + buildRequest(filtered) + "&lang=de&distunit=KM&routepref=Fastest&avoidAreas=&useTMC=false&noMotorways=false&noTollways=false&instructions=false",
                        String.class);
                result = response.getBody().replaceFirst("<xls:XLS.+xsd\">", "<xls:XLS>");
                result = result.replaceAll("xls:|gml:|xsi:", "");
                JAXBContext jaxbContext = JAXBContext.newInstance(RoutingDTO.class);
                Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
                routingDTO = Optional.ofNullable((RoutingDTO) unmarshaller.unmarshal(new StringReader(result)));
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            if (!routingDTO.isPresent()) continue;
            if (routingDTO.get().routingResponse == null
                    || routingDTO.get().routingResponse.routingResponseSubclass == null
                    || routingDTO.get().routingResponse.routingResponseSubclass.routeGeometry == null
                    || routingDTO.get().routingResponse.routingResponseSubclass.routeSummary == null) continue;
            Optional<List<String>> serviceRotings = Optional.ofNullable(routingDTO.get().routingResponse.routingResponseSubclass.routeGeometry.gmlLineString.pos);
            if (!serviceRotings.isPresent()) continue;
            for (String point : serviceRotings.get()) {
                String[] crds = point.split(" ");
                coordinates.add(new Coordinate(
                        Double.parseDouble(crds[0]),
                        Double.parseDouble(crds[1])
                ));
            }
            LineString lineString = factory.createLineString(coordinates.toArray(new Coordinate[coordinates.size()]));
            genericDao.persist(new RoutesPathsEntity(
                    routingDTO.get().routingResponse.routingResponseSubclass.routeSummary.totalTime,
                    Float.parseFloat(routingDTO.get().routingResponse.routingResponseSubclass.routeSummary.totalDistance.value),
                    2,
                    lineString,
                    routesEntity
            ));
//            routesEntity.setGeom(lineString);
            coordinates.clear();
            ++i;
        }
    }

    private String buildRequest(List<RoutesTracksEntity> routesTracks) {
        String req = "Start=" + routesTracks.get(0).getBusStop().getGeom().getX() + ","
                + routesTracks.get(0).getBusStop().getGeom().getY() + "&End="
                + routesTracks.get(routesTracks.size() - 1).getBusStop().getGeom().getX() + ","
                + routesTracks.get(routesTracks.size() - 1).getBusStop().getGeom().getY();
        req += "&Via=";
        String via = "";
        for (int i = 1; i < routesTracks.size() - 1; ++i) {
            via += " " + routesTracks.get(i).getBusStop().getGeom().getX() + "," + routesTracks.get(i).getBusStop().getGeom().getY();
        }
        req += via.trim();
        return req;
    }

    @Transactional
    public void updateGeoms(Set<BusStopsEntity> busStops) throws IOException {
        InputStream in = getClass().getResourceAsStream("/6/przystanki.txt");
        Charset charset = Charset.forName("UTF-8");
        BufferedReader br = new BufferedReader(new InputStreamReader(in, charset));

        Map<String, Integer> stops = new LinkedHashMap<>();
        String[] splitted;
        //Get stops
        String line;
        while ((line = br.readLine()) != null) {
            line = line.replaceFirst(" ", ";");
            splitted = line.split(";");
            stops.put(splitted[1], Integer.parseInt(splitted[0]));
        }
        br.close();
        in.close();

        in = getClass().getResourceAsStream("/6/przystankiwsp.txt");
        br = new BufferedReader(new InputStreamReader(in, charset));
        final GeometryFactory factory = new GeometryFactory(new PrecisionModel(), 4326);
        Map<Integer, Point> coords = new LinkedHashMap<>();
        //Get stops
        while ((line = br.readLine()) != null) {
            splitted = line.split(" ");
            String[] crds = splitted[1].split(";");
            coords.put(Integer.parseInt(splitted[0]), factory.createPoint(
                            new Coordinate(
                                    Double.parseDouble(
                                            String.format("%.6f",Double.parseDouble(crds[0].replaceFirst("(\\d{2})", "$1."))).replace(",",".")
                                    ),
                                    Double.parseDouble(
                                            String.format("%.6f", Double.parseDouble(crds[1].replaceFirst("(\\d{2})", "$1."))).replace(",", ".")
                                    )
                            )
                    )
            );
        }
        br.close();
        in.close();

        for (BusStopsEntity bs : busStops) {
            bs.setGeom(coords.get(stops.get(bs.getName())));
        }

        String bla = "";
    }

    @Transactional
    @SuppressWarnings("unchecked")
    public void traversingGraph() {
        final String sql = "SELECT n.gid, n.name, n.community_id, n.type, e.start_point, e.end_point, e.direction FROM geoportal.bus_stops n "
                + "LEFT JOIN geoportal.connections e ON n.gid = e.end_point "
                + "WHERE e.start_point = :gid";

        final String sqlConnections = "SELECT start_point, end_point, direction FROM geoportal.connections";

        List<BusStopsEntity> busStops =
                genericDao.getAll(BusStopsEntity.class);
        Map<String, Object> params = new HashMap<>();
        params.put("gid", 0);
//        Map<Integer, List> results = new HashMap<>();
//        for (BusStopsEntity busStopsEntity : busStops) {
//            params.put("gid", busStopsEntity.getGid());
//            results.put(busStopsEntity.getGid(), genericDao.runNativeQuery(sql, params, Criteria.ALIAS_TO_ENTITY_MAP));
//        }

//        List<Double> distance = new ArrayList<>();
//        results
//                .entrySet()
//                .stream()
//                .forEach((k) -> {
//                            List test = k.getValue();
//                            for (HashMap map : (List<HashMap>) test) {
//                                int startPoint = (Integer) map.get("start_point");
//                                int endPoint = (Integer) map.get("end_point");
//
//                                Point startPointGeom = busStops.stream()
//                                        .filter(p -> p.getGid() == startPoint)
//                                        .findFirst().get().getGeom();
//                                Point endPointGeom = busStops.stream()
//                                        .filter(p -> p.getGid() == endPoint)
//                                        .findFirst().get()
//                                        .getGeom();
//
//
//                                distance.add(
//                                        calculateDistance(
//                                                startPointGeom.getX(),
//                                                startPointGeom.getY(),
//                                                endPointGeom.getX(),
//                                                endPointGeom.getY()
//                                        )
//                                );
//                            }
//                        }
//                );

//        List connections = genericDao.runNativeQuery(sqlConnections, new HashMap<>(), Criteria.ALIAS_TO_ENTITY_MAP);
//        HashSet<SimpleEdge> simpleEdges = new HashSet<>();

//        for (HashMap p : (List<HashMap>) connections) {
//            if (p.get("direction").equals(1)) {
//                simpleEdges.add(new SimpleEdge((Integer) p.get("start_point"), (Integer) p.get("end_point")));
//            } else {
//                simpleEdges.add(new SimpleEdge((Integer) p.get("end_point"), (Integer) p.get("start_point")));
//            }
//        }


//        List<Osm2PoJsonDTO> jsonDTOs = getDistanceAndLenght(simpleEdges, busStops);

        String bla = "";
    }

    @Transactional
    private List<Osm2PoJsonDTO> getDistanceAndLenght(Set<SimpleEdge> simpleEdgeHashMap, List<BusStopsEntity> busStopsEntities) {
        final String update = "UPDATE geoportal.connections SET travel_time=:time, distance=:km_distance "
                + "WHERE start_point=:start AND end_point=:end";

        RestTemplate routingWebService = new RestTemplate();
        List<HttpMessageConverter<?>> messageConverterList = routingWebService.getMessageConverters();
        MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
        List<MediaType> supportedMediaTypes = new ArrayList<>();
        supportedMediaTypes.add(new MediaType("text", "plain"));
        supportedMediaTypes.add(new MediaType("application", "json"));
        supportedMediaTypes.add(MediaType.TEXT_HTML);

        converter.setSupportedMediaTypes(supportedMediaTypes);
        messageConverterList.add(converter);
        routingWebService.setMessageConverters(messageConverterList);
        HashSet<BusStopsEntity> wrongOnes = new HashSet<>();

        Point startPointGeom;
        Point endPointGeom;
        double time = 0.0;
        double distance = 0.0;

        Map<String, Object> params = new HashMap<>();

        List<Osm2PoJsonDTO> osm2PoJsonDTOs = new ArrayList<>();
        for (SimpleEdge simpleEdge : simpleEdgeHashMap) {
            startPointGeom = busStopsEntities.stream()
                    .filter(p -> Objects.equals(p.getGid(), simpleEdge.getStartPoint()))
                    .findFirst().get().getGeom();
            endPointGeom = busStopsEntities.stream()
                    .filter(p -> Objects.equals(p.getGid(), simpleEdge.getEndPoint()))
                    .findFirst().get()
                    .getGeom();

            String result = routingWebService.getForObject(
                    "http://localhost:8888/Osm2poService?format=geojson&cmd=fr&" +
                            "source=" + startPointGeom.getY() + "," + startPointGeom.getX() + "&" +
                            "target=" + endPointGeom.getY() + "," + endPointGeom.getX() + "&" +
                            "findShortestPath=true&ignoreRestrictions=false&ignoreOneWays=false&routerClassId=0&heuristicFactor=0.0&maxCost=0.0",
                    String.class
            );

            if (result.contains("TargetId")) {
                wrongOnes.add(busStopsEntities.stream()
                        .filter(p -> Objects.equals(p.getGid(), simpleEdge.getEndPoint()))
                        .findFirst().get());
                continue;
            }

            if (result.contains("SourceId")) {
                wrongOnes.add(busStopsEntities.stream()
                        .filter(p -> Objects.equals(p.getGid(), simpleEdge.getStartPoint()))
                        .findFirst().get());
                continue;
            }

            if (result.contains("Error")) {
                continue;
            }

            Osm2PoJsonDTO response = routingWebService.getForObject(
                    "http://localhost:8888/Osm2poService?format=geojson&cmd=fr&" +
                            "source=" + startPointGeom.getY() + "," + startPointGeom.getX() + "&" +
                            "target=" + endPointGeom.getY() + "," + endPointGeom.getX() + "&" +
                            "findShortestPath=true&ignoreRestrictions=false&ignoreOneWays=false&routerClassId=0&heuristicFactor=0.0&maxCost=0.0",
                    Osm2PoJsonDTO.class
            );

            osm2PoJsonDTOs.add(response);
            for(OsmFeaturesDTO osmFeaturePropertiesDTO : response.getOsmFeaturesDTOs()){
                time += osmFeaturePropertiesDTO.getOsmFeaturesDTO().getTime();
                distance += osmFeaturePropertiesDTO.getOsmFeaturesDTO().getLength();
            }

            params.put("time", time);
            params.put("km_distance", distance);
            params.put("start", simpleEdge.getStartPoint());
            params.put("end", simpleEdge.getEndPoint());

            genericDao.runNativeQuery(update, params);
//
//            params.put("start", simpleEdge.getEndPoint());
//            params.put("end", simpleEdge.getStartPoint());
            distance = 0.0;
            time = 0.0;
        }

//        try {
//            updateGeoms(wrongOnes);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
        return osm2PoJsonDTOs;
    }

    public double calculateDistance(double userLat, double userLng,
                                    double venueLat, double venueLng) {

        double latDistance = Math.toRadians(userLat - venueLat);
        double lngDistance = Math.toRadians(userLng - venueLng);

        double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2)
                + Math.cos(Math.toRadians(userLat)) * Math.cos(Math.toRadians(venueLat))
                * Math.sin(lngDistance / 2) * Math.sin(lngDistance / 2);

        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

        return Math.round(AVERAGE_RADIUS_OF_EARTH * c * 1000);
    }


    public void updateTimes(){

    }
}


@Data
class SimpleEdge {
    private final Integer startPoint;
    private final Integer endPoint;

    public SimpleEdge(Integer startPoint, Integer endPoint) {
        this.startPoint = startPoint;
        this.endPoint = endPoint;
    }
}


class Edge {

    private final Integer startPoint;
    private final Integer endPoint;
    private final Integer direction;


    public static Edge createPair(Integer startPoint, Integer endPoint, Integer direction) {
        return new Edge(startPoint, endPoint, direction);
    }

    public Edge(Integer startPoint, Integer endPoint, Integer direction) {
        this.startPoint = startPoint;
        this.endPoint = endPoint;
        this.direction = direction;
    }

    public Integer getStartPoint() {
        return startPoint;
    }

    public Integer getEndPoint() {
        return endPoint;
    }

    public Integer getDirection() {
        return direction;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Edge edge = (Edge) o;

        if (startPoint != null ? !startPoint.equals(edge.startPoint) : edge.startPoint != null) return false;
        if (endPoint != null ? !endPoint.equals(edge.endPoint) : edge.endPoint != null) return false;
        return !(direction != null ? !direction.equals(edge.direction) : edge.direction != null);

    }

    @Override
    public int hashCode() {
        int result = startPoint != null ? startPoint.hashCode() : 0;
        result = 31 * result + (endPoint != null ? endPoint.hashCode() : 0);
        result = 31 * result + (direction != null ? direction.hashCode() : 0);
        return result;
    }
}
