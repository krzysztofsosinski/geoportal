package pl.geoportal.client;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class BusStopsWrapper {
    
    @JsonProperty("markers")
    public List<BusStopDTO> busStopDTOs = new ArrayList<>();

    public List<BusStopDTO> getBusStopDTOs() {
        return busStopDTOs;
    }
}
