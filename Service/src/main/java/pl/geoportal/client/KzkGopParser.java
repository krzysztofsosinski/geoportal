package pl.geoportal.client;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.geoportal.model.*;
import pl.geoportal.persistence.GenericDao;

import javax.transaction.Transactional;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Component
@Transactional
public class KzkGopParser {

    @Autowired
    GenericDao<CalendarEntity, Integer> calendarDao;

    @Autowired
    GenericDao<BusStopsEntity, Integer> busStopsHelper;


    @SuppressWarnings("unchecked")
    public void parseTransportLines() {
        try {
            InputStream in = getClass().getResourceAsStream("/6/przystanki.txt");
            Charset charset = Charset.forName("UTF-8");
            BufferedReader br = new BufferedReader(new InputStreamReader(in, charset));
            //        busStopsHelper.senEntityClass(BusStopsEntity.class);
            //      List<BusStopsEntity> stopsEntities = (List<BusStopsEntity> )busStopsHelper.getList();
            //          busStopsHelper.senEntityClass(StopTypeEntity.class);
//            List<StopTypeEntity> stopTypeEntities = (List<StopTypeEntity>)  busStopsHelper.getList();

//            Map<Integer, String> stops = new LinkedHashMap<>();
//            Map<String, String> fileLines = new LinkedHashMap<>();
//            List<String> files = new ArrayList<>();
//            String[] splitted;
//            Map<TripWrapper, List<LineWrapper>> result = new LinkedHashMap<>();
//            //Get stops
            String line;
//            while ((line = br.readLine()) != null) {
//                line = line.replaceFirst(" ", ";");
//                splitted = line.split(";");
//                stops.put(Integer.parseInt(splitted[0]), splitted[1]);
//            }
//            br.close();
//            in.close();
//
//            Set<String> linesToParse = new HashSet<>();
//            in = getClass().getResourceAsStream("/6/linie_smaller.txt");
//            br = new BufferedReader(new InputStreamReader(in, charset));
//            while ((line = br.readLine()) != null) {
//                linesToParse.add(line);
//            }
//            br.close();
//            in.close();


//            Pattern p1 = Pattern.compile("var poli.+;polilinia");
//            Matcher m1;
//            List<String> markers;
//            final List<String> poly1 = new LinkedList<>();
//            final List<String> poly2 = new LinkedList<>();
//            final Set<String> opt = new HashSet<>();
//            for (String busLine : linesToParse) {
//                Document doc = Jsoup.connect("http://rozklady.kzkgop.pl/index.php?co=mapa&akcja=pokaz_trasy&numer_linii="+busLine).get();
//                Elements scriptElements = doc.getElementsByTag("script");
//
//                for (Element element : scriptElements) {
//                    for (DataNode node : element.dataNodes()) {
//                        m1 = p1.matcher(node.getWholeData());
//                        while (m1.find()) {
//                            markers = Arrays.asList(m1.group().split(";"));
//                            for (int i = 0; i < markers.size(); ++i) {
//                                if (markers.get(i).equals("polilinia")) break;
//                                if (markers.get(i).contains("add_marker") && markers.get(i + 1).contains("poli.push")) {
//                                    poly1.add(markers.get(i).split(",")[2].trim().replaceAll("'", "").replace(" n/ż",""));
//                                } else if (markers.get(i).contains("add_marker") && markers.get(i + 1).contains("poli2.push")) {
//                                    poly2.add(markers.get(i).split(",")[2].trim().replaceAll("'", "").replace(" n/ż",""));
//                                } else if (markers.get(i).contains("add_marker") && markers.get(i + 1).contains("add_marker")) {
//                                    opt.add(markers.get(i).split(",")[2].trim().replaceAll("'", "").replace(" n/ż",""));
//                                }
//                            }
//                        }
//                    }
//                }
//                saveTracks(poly1, poly2, opt, busLine);
//                poly1.clear();
//                poly2.clear();
//                opt.clear();
//            }
//            for(String busStopName : stops.values()){
//                Optional<BusStopsEntity> value = stopsEntities
//                        .stream()
//                        .filter(b -> Objects.equals(b.getName(), busStopName))
//                        .findFirst();
//                try{
//                    value.get();
//                }catch (NoSuchElementException ex){
//                    busStopsHelper.persist(new BusStopsEntity(busStopName, -1, stopTypeEntities.get(0)));
//                    System.err.println(ex.getMessage());
//                }
//            }

            //Get line files
//            in = getClass().getResourceAsStream("/6/linie_re.txt");
//            br = new BufferedReader(new InputStreamReader(in, charset));
//            while ((line = br.readLine()) != null) {
//                if (line.equals("S-6-0.txt") || line.equals("S-6-1.txt")
//                        || line.equals("S-10-0.txt") || line.equals("S-10-1.txt")
//                        || line.equals("T-15-1.txt") || line.equals("T-15-0.txt")
//                        || line.equals("T-14-1.txt") || line.equals("T-14-0.txt")) {
//                    String temp = line.replaceFirst("-", ";");
//                    splitted = temp.split("-");
//                    splitted[0] = splitted[0].replaceFirst(";", "-");
//                    fileLines.put(splitted[0], line);
//                    files.add(line);
//                } else {
//                    splitted = line.split("-");
//                    fileLines.put(splitted[0], line);
//                    files.add(line);
//                }
//            }
//            br.close();
//            in.close();

            //Get line files
            Set<String> linesToParse = new HashSet<>();
            in = getClass().getResourceAsStream("/6/linie_smaller.txt");
            br = new BufferedReader(new InputStreamReader(in, charset));
            while ((line = br.readLine()) != null) {
                linesToParse.add(line);
            }
            br.close();
            in.close();

            Document doc;
            Map<String, List<BusStop>> left = new HashMap<>();
            Map<String, List<BusStop>> right = new HashMap<>();
            for (String transportLine : linesToParse) {
                doc = Jsoup.connect("http://77.252.189.162/index.php?co=rozklady&submenu=trasy&nr_linii=" + transportLine).get();
                Elements leftLinks = doc.select("#lewo a");
                Elements rightLinks = doc.select("#prawo a");
                List<BusStop> leftStops = new ArrayList<>();
                List<BusStop> rightStops = new ArrayList<>();
                leftLinks.forEach(el -> leftStops.add(new BusStop(el.text(), "http://77.252.189.162/" + el.attr("href"))));
                left.put(transportLine, leftStops);
                rightLinks.forEach(el -> rightStops.add(new BusStop(el.text(), "http://77.252.189.162/"+ el.attr("href"))));
                right.put(transportLine, rightStops);
            }

//            for (String file : files) {
//                result = parseLineTimetable(result, file);
//            }
//            saveTimetable(result, stops);

            List<TempLinksEntity> linksEntitiesRight = new ArrayList<>();
            for (Map.Entry<String, List<BusStop>> entry : right.entrySet()) {
                for (BusStop busStop : entry.getValue()) {
                    doc = Jsoup.connect(busStop.href).get();
                    Elements days = doc.select("tr[class^=typ_dnia]");

                    for (Element el : days) {
                        String type = el.child(0).text();
                        Elements links = el.siblingElements().get(el.elementSiblingIndex()).getElementsByTag("a");
                        for (Element link : links) {
                            String href = link.attr("href");
                            if (href.contains("nr_przystanku=1#")) {
                                linksEntitiesRight.add(new TempLinksEntity(type, "http://77.252.189.162/" + href, entry.getKey(), 2));
                            }
                        }
                    }
                }
            }

            saveLinks(linksEntitiesRight);

            List<TempLinksEntity> linksEntitiesLeft = new ArrayList<>();
            for (Map.Entry<String, List<BusStop>> entry : left.entrySet()) {
                for (BusStop busStop : entry.getValue()) {
                    doc = Jsoup.connect(busStop.href).get();
                    Elements days = doc.select("tr[class^=typ_dnia]");
                    for (Element el : days) {
                        String type = el.child(0).text();
                        Elements links = el.siblingElements().get(el.elementSiblingIndex()).getElementsByTag("a");
                        for (Element link : links) {
                            String href = link.attr("href");
                            if (href.contains("nr_przystanku=1#")) {
                                linksEntitiesLeft.add(new TempLinksEntity(type, "http://77.252.189.162/" + href, entry.getKey(), 1));
                            }
                        }
                    }
                }
            }
            saveLinks(linksEntitiesLeft);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void saveLinks(List<TempLinksEntity> links) {
        links.forEach(busStopsHelper::persist);
    }

    private class BusStop {
        public String name;
        public String href;

        public BusStop(String name, String href) {
            this.name = name;
            this.href = href;
        }
    }

    private class LineWrapper {
        int type;
        int index;
        List<Time> times;

        public LineWrapper() {
            times = new LinkedList<>();
        }

        public LineWrapper(int type, int index, List<Time> times) {
            this.type = type;
            this.index = index;
            this.times = times;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            LineWrapper that = (LineWrapper) o;

            if (index != that.index) return false;
            if (type != that.type) return false;
            if (times != null ? !times.equals(that.times) : that.times != null) return false;

            return true;
        }

        @Override
        public int hashCode() {
            int result = type;
            result = 31 * result + index;
            result = 31 * result + (times != null ? times.hashCode() : 0);
            return result;
        }
    }

    private class TripWrapper {
        String name;
        String headsign;
        String tripName;
        int direction;
        int type;

        public TripWrapper(String name, String headsign, String tripName, int direction, int type) {
            this.name = name;
            this.headsign = headsign;
            this.tripName = tripName;
            this.direction = direction;
            this.type = type;
        }

        public TripWrapper(String name, int direction, int type) {
            this.name = name;
            this.direction = direction;
            this.type = type;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            TripWrapper that = (TripWrapper) o;

            if (direction != that.direction) return false;
            if (type != that.type) return false;
            if (headsign != null ? !headsign.equals(that.headsign) : that.headsign != null) return false;
            if (name != null ? !name.equals(that.name) : that.name != null) return false;
            if (tripName != null ? !tripName.equals(that.tripName) : that.tripName != null) return false;

            return true;
        }

        @Override
        public int hashCode() {
            int result = name != null ? name.hashCode() : 0;
            result = 31 * result + (headsign != null ? headsign.hashCode() : 0);
            result = 31 * result + (tripName != null ? tripName.hashCode() : 0);
            result = 31 * result + direction;
            result = 31 * result + type;
            return result;
        }
    }

    private Map<TripWrapper, List<LineWrapper>> parseLineTimetable(Map<TripWrapper, List<LineWrapper>> content, String fileName) {
        try {
            final String CLONE = "JAKWYZEJ";
            final String SKIP = "BRAK";
            Pattern p1 = Pattern.compile("[0-9]{3}");
            Pattern p2 = Pattern.compile("[0-9]{4}");
            Pattern p3 = Pattern.compile("^T[0-9]{1,2}");
            SimpleDateFormat sdf1 = new SimpleDateFormat("HHmm");

            InputStream in = getClass().getResourceAsStream("/6/" + fileName);
            Charset charset = Charset.forName("UTF-8");
            BufferedReader br = new BufferedReader(new InputStreamReader(in, charset));
            String line;
            String name = "";
            String startPlace = "";
            String endPlace = "";

            int index = 0;
            int counter = 1;
            int type = 0;
            List<LineWrapper> stops = new LinkedList<>();
            Matcher m3;
            for (; (line = br.readLine()) != null; ++counter) {
                if (counter == 1) {
                    name = line;
                    continue;
                } else if (counter == 2) {
                    startPlace = line;
                    continue;
                } else if (counter == 3) {
                    endPlace = line;
                    continue;
                } else if (counter <= 3 || line.equals(SKIP)) {
                    continue;
                }

                //Real parsing
                if (counter % 4 == 0) {
                    index = Integer.parseInt(line);
                    continue;
                }

                if (counter % 5 == 0) type = 1; //PON - PT
                if (counter % 6 == 0) type = 2; //SOB
                if (counter % 7 == 0) type = 3; //ND


                if (line.equals(CLONE)) {
                    LineWrapper lineWrapper = stops.get(stops.size() - 1);
                    lineWrapper.type = type;
                    stops.add(lineWrapper);
                    continue;
                }
                String[] times = line.split(",");
                Matcher m1, m2;
                List<Time> tms = new LinkedList<>();
                for (String tm : times) {
                    m1 = p1.matcher(tm);
                    m2 = p2.matcher(tm);

                    //4 numbers
                    if (m2.find()) {
                        Date date = sdf1.parse(m2.group());
                        tms.add(new Time(date.getTime()));
                    }
                    //3 numbers
                    else if (m1.find()) {
                        Date date = sdf1.parse("0" + m1.group());
                        tms.add(new Time(date.getTime()));
                    }

                }
                stops.add(new LineWrapper(type, index, tms));
            }
            m3 = p3.matcher(fileName);
            content.put(
                    new TripWrapper(name,
                            startPlace,
                            startPlace + " -> " + endPlace,
                            (fileName.contains("-1")) ? 1 : 0,
                            (m3.find()) ? 2 : 1),
                    stops);

            br.close();
            in.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return content;
    }

    @SuppressWarnings("unchecked")
    private void saveTimetable(Map<TripWrapper, List<LineWrapper>> content, Map<Integer, String> stops) {
        calendarDao.senEntityClass(CalendarEntity.class);
        final Map<Integer, CalendarEntity> calendars = prepareConnections((List<CalendarEntity>) calendarDao.getList());
        busStopsHelper.senEntityClass(BusStopsEntity.class);
        final Map<String, BusStopsEntity> busStops = prepareConnections((List<BusStopsEntity>) busStopsHelper.getList());
        busStopsHelper.senEntityClass(RoutesEntity.class);
        final Map<String, RoutesEntity> routes = prepareConnections((List<RoutesEntity>) busStopsHelper.getList());
        final List<RoutesEntity> routesFromDb = (List<RoutesEntity>) busStopsHelper.getList();
        final int NUM_OF_THREADS = Runtime.getRuntime().availableProcessors();

        List<RoutesEntity> routesEntities = new ArrayList<>();
        List<TripsEntity> tripsEntitiesFinal = Collections.synchronizedList(new ArrayList<>());
        List<StopTimesEntity> stopTimesEntities = Collections.synchronizedList(new ArrayList<>());


        List<TimetableEntity> timetableEntities = new ArrayList<>();
        List<LineWrapper> wrongOnes = new ArrayList<>();
        Set<String> wrongIndexes = new HashSet<>();
        ExecutorService exec = Executors.newFixedThreadPool(NUM_OF_THREADS * 2);
        //  try {
        for (Map.Entry<TripWrapper, List<LineWrapper>> result : content.entrySet()) {
            //exec.submit(() -> {
//                    List<TripsEntity> tripsEntities = new ArrayList<>();
            TripWrapper temp = result.getKey();
            RoutesEntity parent = new RoutesEntity(new Integer(temp.type).shortValue(), temp.name);
//            try{
//            routesFromDb.stream()
//                    .filter(r -> Objects.equals(r.getRouteName(), parent.getRouteName()))
//                    .findFirst().get();
//            }catch (NoSuchElementException ex){
//                busStopsHelper.persist(parent);
//            }
//
//            routesEntities.add(parent);

            result.getValue().forEach(line -> line.times.forEach(
                    time -> {
                        busStopsHelper.persist(new TimetableEntity(
                                        time, temp.direction,
                                        temp.headsign,
                                        "", busStops.get(stops.get(line.index)),
                                        routes.get(temp.name),
                                        calendars.get(line.type)
                                )
                        );
                    }
            ));

//                    int counter = 1;
//                    for (LineWrapper line : result.getValue()) {
//                        if (counter == 1) {
//
//                            for (Time time : line.times) {
//
//                                TripsEntity tripsEntity = new TripsEntity(
//                                        new Integer(temp.direction).shortValue(), temp.tripName,
//                                        temp.headsign, parent, calendars.get(line.type));
//                                tripsEntities.add(tripsEntity);
//
//                                StopTimesEntity stopTimesEntity = new StopTimesEntity(
//                                        time, time, counter, tripsEntity, busStops.get(stops.get(line.index))
//                                );
//                                tripsEntity.getStopTimesesByTripId().add(stopTimesEntity);
//                            }
//                        } else {
//                            for (int i = 0; i < line.times.size(); ++i) {
//                                Time timeTemp = line.times.get(i);
//
//                                StopTimesEntity stopTimesEntity = new StopTimesEntity(
//                                        timeTemp, timeTemp, counter, null, busStops.get(stops.get(line.index))
//                                );
//                                if (i == tripsEntities.size()) {
//                                    TripsEntity tripsEntity = new TripsEntity(
//                                            new Integer(temp.direction).shortValue(),
//                                            temp.tripName, temp.headsign, parent, calendars.get(line.type)
//                                    );
//                                    stopTimesEntity.setTripsByTripId(tripsEntity);
//                                    tripsEntities.add(tripsEntity);
//                                } else {
//                                    stopTimesEntity.setTripsByTripId(tripsEntities.get(i));
//                                    tripsEntities.get(i).getStopTimesesByTripId().add(stopTimesEntity);
//                                }
//                            }
//                        }
//                        ++counter;
//                    }
//                    parent.setTripsesByRouteId(tripsEntities);
        }
        //  }
        //} finally {
        //  exec.shutdown();
        //}


//        routesEntities.forEach(calendarDao::persist);
//        tripsEntitiesFinal.forEach(calendarDao::persist);
//        stopTimesEntities.forEach(calendarDao::persist);

        //  saveRoutes(routesEntities);
        //  saveTimetables(timetableEntities);

        String bla = "asdsa";
    }

    @SuppressWarnings("unchecked")
    private void saveTracks(final List<String> poly1, final List<String> poly2, Set<String> opt, final String line) {
        busStopsHelper.senEntityClass(RoutesEntity.class);
        List<RoutesEntity> lineEntity = ((List<RoutesEntity>) busStopsHelper.getList());
        final Map<String, RoutesEntity> bla = lineEntity
                .stream()
                .collect(Collectors.toMap(RoutesEntity::getRouteName, Function.<RoutesEntity>identity()));
        busStopsHelper.senEntityClass(BusStopsEntity.class);
        final Map<String, BusStopsEntity> nameBusStop = ((List<BusStopsEntity>) busStopsHelper.getList())
                .stream()
                .collect(Collectors.toMap(BusStopsEntity::getName, x -> x));


        for (int i = 0, poly1Size = poly1.size(); i < poly1Size; i++) {
            String aPoly1 = poly1.get(i);
            Optional<BusStopsEntity> bs = Optional.ofNullable(nameBusStop.get(aPoly1));
            Optional<RoutesEntity> re = Optional.ofNullable(bla.get(line));
            busStopsHelper.persist(
                    new RoutesTracksEntity(i, 1, nameBusStop.get(aPoly1), bla.get(line))
            );

        }

        for (int i = 0, poly2Size = poly2.size(); i < poly2Size; i++) {
            String aPoly2 = poly2.get(i);
            Optional<BusStopsEntity> bs = Optional.ofNullable(nameBusStop.get(aPoly2));
            Optional<RoutesEntity> re = Optional.ofNullable(bla.get(line));
            busStopsHelper.persist(
                    new RoutesTracksEntity(i, 2, nameBusStop.get(aPoly2), bla.get(line))
            );

        }

        for (String optPoly : opt) {
            Optional<BusStopsEntity> bs = Optional.ofNullable(nameBusStop.get(optPoly));
            Optional<RoutesEntity> re = Optional.ofNullable(bla.get(line));
            busStopsHelper.persist(new RoutesTracksEntity(-1, 1, nameBusStop.get(optPoly), bla.get(line)));

        }

    }

    private void saveRoutes(List<RoutesEntity> links) {
        links.forEach(busStopsHelper::persist);
    }

    private void saveTimetables(List<TimetableEntity> links) {
        links.forEach(busStopsHelper::persist);
    }


    @SuppressWarnings("unchecked")
    private <T, K> Map<K, T> prepareConnections(List<T> entities) {
        Map<K, T> result = new HashMap<>();
        for (Object entity : entities) {
            if (entity instanceof CalendarEntity) {
                result.put((K) ((CalendarEntity) entity).getServiceId(), (T) entity);
            } else if (entity instanceof BusStopsEntity) {
                result.put((K) ((BusStopsEntity) entity).getName(), (T) entity);
            } else if (entity instanceof RoutesEntity) {
                result.put((K) ((RoutesEntity) entity).getRouteName(), (T) entity);
            }
        }
        return result;
    }

}
