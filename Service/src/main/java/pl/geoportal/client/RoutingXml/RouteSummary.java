package pl.geoportal.client.RoutingXml;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = {"totalTime", "totalDistance"})
public class RouteSummary {

    @XmlElement(name = "TotalTime")
    public String totalTime;

    @XmlElement(name = "TotalDistance")
    public TotalDistance totalDistance;
}
