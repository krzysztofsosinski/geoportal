package pl.geoportal.client.RoutingXml;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import java.util.List;

@XmlAccessorType(XmlAccessType.FIELD)
public class GmlLineString {

    @XmlAttribute
    public String srsName;

    @XmlElement(name = "pos")
    public List<String> pos;

}
