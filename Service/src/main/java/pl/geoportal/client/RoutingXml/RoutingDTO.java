package pl.geoportal.client.RoutingXml;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "XLS")
@XmlAccessorType(XmlAccessType.FIELD)
public class RoutingDTO {

    @XmlElement(name = "Response")
    public RoutingResponse routingResponse;

//    @XmlAttribute(name = "xmlns:xls")
//    public String xls;
//
//    @XmlAttribute(name = "xmlns:xsi")
//    public String xsi;
//
//    @XmlAttribute(name = "xmlns:gml")
//    public String gml;
//
//    @XmlAttribute
//    public String version;
//
//    @XmlAttribute(name = "xsi:schemaLocation")
//    public String schemaLocation;

}
