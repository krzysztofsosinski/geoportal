package pl.geoportal.client.RoutingXml;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = {"routeSummary", "routeGeometry"})
public class RoutingResponseSubclass {

    @XmlElement(name = "RouteSummary")
    public RouteSummary routeSummary;

    @XmlElement(name = "RouteGeometry")
    public RouteGeometry routeGeometry;

}
