package pl.geoportal.client.RoutingXml;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;

@XmlAccessorType(XmlAccessType.FIELD)
public class TotalDistance {

    @XmlAttribute
    public String uom;

    @XmlAttribute
    public String value;
}
