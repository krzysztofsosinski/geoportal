package pl.geoportal.client.RoutingXml;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

@XmlAccessorType(XmlAccessType.FIELD)
public class RoutingResponse {

    @XmlElement(name = "DetermineRouteResponse")
    public RoutingResponseSubclass routingResponseSubclass;
}
