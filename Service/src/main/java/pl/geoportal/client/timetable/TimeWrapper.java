package pl.geoportal.client.timetable;

import lombok.Data;

import java.sql.Time;

@Data
public class TimeWrapper {

    private Time time;
    private String annotation;

}
