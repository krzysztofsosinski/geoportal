package pl.geoportal.client.timetable;

import lombok.Data;

import java.util.List;

@Data
public class RouteHelper {
    private final int routeId;
    private final List<TrackWrapper> left;
    private final List<TrackWrapper> right;
}
