package pl.geoportal.client.timetable;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class BusStopWrapper {

    private Integer busStopId;
    private List<TimeWrapper> workingDays = new ArrayList<>();
    private List<TimeWrapper> saturdays = new ArrayList<>();
    private List<TimeWrapper> sundays = new ArrayList<>();

}
