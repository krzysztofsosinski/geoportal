package pl.geoportal.client.timetable;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@EqualsAndHashCode
public class RouteWrapper {

    @Getter @Setter private int routeId;
    @Getter @Setter private String name;
    @Getter @Setter private int direction;
    @Getter @Setter private List<BusStopWrapper> track;

    public RouteWrapper() {
        routeId = Integer.MIN_VALUE;
        name = "";
        direction = 1;
    }

    public RouteWrapper(int routeId, String name, int direction){
        this.routeId= routeId;
        this.name = name;
        this.direction = direction;
    }

    public RouteWrapper(int routeId, String name, int direction, List<BusStopWrapper> track){
        this.routeId= routeId;
        this.name = name;
        this.direction = direction;
        this.track = track;
    }
}
