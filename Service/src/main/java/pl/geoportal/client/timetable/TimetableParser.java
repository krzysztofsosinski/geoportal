package pl.geoportal.client.timetable;

import lombok.Cleanup;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import pl.geoportal.model.Connections;
import pl.geoportal.model.RoutesEntity;
import pl.geoportal.model.SimplifiedTracksEntity;
import pl.geoportal.model.TempLinksEntity;
import pl.geoportal.persistence.BusStopsDao;
import pl.geoportal.persistence.GenericDao;
import pl.geoportal.persistence.SimplifiedTrackDao;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Component
public class TimetableParser {

    @Autowired
    BusStopsDao busStopDao;

    @Autowired
    GenericDao<RoutesEntity, Integer> genericDao;

    @Autowired
    GenericDao<TempLinksEntity, Integer> tempLinksDao;

    @Autowired
    SimplifiedTrackDao trackDao;

    private final String CLONE = "JAKWYZEJ";
    private final String SKIP = "BRAK";

    private final SimpleDateFormat sdf = new SimpleDateFormat("HHmm");

    private final Pattern p1 = Pattern.compile("[0-9]{3}(.{2})?");
    private final Pattern p2 = Pattern.compile("[0-9]{4}(.{2})?");
    private final Pattern p3 = Pattern.compile("^T[0-9]{1,2}");

    private final Charset charset = Charset.forName("UTF-8");

    class Connection {
        final int start;
        final int end;
        final int dir;
        final int route_id;

        public Connection(int start, int end, int dir, int route_id) {
            this.start = start;
            this.end = end;
            this.dir = dir;
            this.route_id = route_id;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Connection that = (Connection) o;
            return Objects.equals(start, that.start) &&
                    Objects.equals(end, that.end) &&
                    Objects.equals(dir, that.dir) &&
                    Objects.equals(route_id, that.route_id);
        }

        @Override
        public int hashCode() {
            return Objects.hash(start, end, dir, route_id);
        }
    }

    @Transactional
    public void createExtConnections() {
        List<RoutesEntity> routesEntities = genericDao.getAll(RoutesEntity.class);
        final String sql3 = "INSERT INTO geoportal.ext_connections (start_point, end_point, direction, route_id) " +
                "VALUES(:start, :end, :dir, :route)";
        Map<String, Object> params = new HashMap<>();
        Set<Connection> cons = new HashSet<>();
        for (RoutesEntity routesEntity : routesEntities) {
            List<SimplifiedTracksEntity> tracksLeft = trackDao.getByRouteAndDirection(routesEntity.getRouteId(), 1);
            List<SimplifiedTracksEntity> tracksRight = trackDao.getByRouteAndDirection(routesEntity.getRouteId(), 2);

            if (!tracksLeft.isEmpty())
                try {
                    for (int i = 0; i < tracksLeft.size() - 2; ++i) {
                        int j = i + 2;
                        if (j > tracksLeft.size() - 1) {
                            continue;
                        }
                        for (; j < tracksLeft.size(); ++j) {
                            cons.add(
                                    new Connection(
                                            tracksLeft.get(i).getBusStop().getGid(),
                                            tracksLeft.get(j).getBusStop().getGid(),
                                            1, routesEntity.getRouteId())
                            );
                        }

                    }
                } catch (IndexOutOfBoundsException ex) {
                    ex.printStackTrace();
                }
            if (!tracksRight.isEmpty())
                try {

                    for (int i = 0; i < tracksRight.size() - 2; ++i) {
                        int j = i + 2;
                        if (j > tracksRight.size() - 1) {
                            continue;
                        }
                        for (; j < tracksRight.size(); ++j) {
                            cons.add(
                                    new Connection(
                                            tracksRight.get(i).getBusStop().getGid(),
                                            tracksRight.get(j).getBusStop().getGid(),
                                            2, routesEntity.getRouteId())
                            );
                        }
                    }
                } catch (IndexOutOfBoundsException ex) {
                    ex.printStackTrace();
                }
        }
        int i = 0;
        for (Connection connection : cons) {
            ++i;
            params.put("start", connection.start);
            params.put("end", connection.end);
            params.put("dir", connection.dir);
            params.put("route", connection.route_id);
            genericDao.runNativeQuery(sql3, params);
        }

    }

    @Transactional
    @SuppressWarnings("unchecked")
    public void createRegularTracks() throws IOException, ParseException {
//        List<TempLinksEntity> links = tempLinksDao.getAll(TempLinksEntity.class);
        List<RoutesEntity> routesEntities = genericDao.getAll(RoutesEntity.class);
//        List<StopTypeEntity> stopTypes = genericDao.getAll(StopTypeEntity.class);

        List<Connections> connectionses = genericDao.getAll(Connections.class);

//        Collections.shuffle(links);
        Map<Integer, RouteHelper> routes = new HashMap<>();

        final String sql = "INSERT INTO geoportal.simplified_tracks (track_order, route_id, bus_stop_id, departure, direction)" +
                " VALUES (:order, :route, :bus, :dep, :dir)";

        final String sql2 = "INSERT INTO geoportal.bus_stops (name, community_id) VALUES (:name, :community_id)";

        final String sql3 = "INSERT INTO geoportal.connections (start_point, end_point, direction, time_mili) " +
                "VALUES(:start, :end, :dir, :milis)";

        for (Connections connections : connectionses) {
            if (connections.getTime() != null
                    && (connections.getTimeMili() == null || connections.getTimeMili() == 0)) {
                connections.setTimeMili((long) (connections.getTime() * 3600000));
            }
        }

        int h = 0;
        Map<String, Object> params = new HashMap<>();
        for (RoutesEntity routesEntity : routesEntities) {
            List<SimplifiedTracksEntity> tracksLeft = trackDao.getByRouteAndDirection(routesEntity.getRouteId(), 1);
            List<SimplifiedTracksEntity> tracksRight = trackDao.getByRouteAndDirection(routesEntity.getRouteId(), 2);
//            List<SimplifiedTracksEntity> all = new ArrayList<>();
//            all.addAll(tracksLeft); all.addAll(tracksRight);
//            boolean exist = true;
//            for(SimplifiedTracksEntity tracksEntity : all){
//
//            }


//            if(!tracksLeft.isEmpty())
//            for (int i = 0; i < tracksLeft.size() - 1; ++i) {
//                Set<Connections> stopsFrom = tracksLeft.get(i).getBusStop().getStopsFrom();
//                for (Connections connections : stopsFrom) {
//
//                    if (connections.getPk().getEnd().getGid().equals(tracksLeft.get(i + 1).getBusStop().getGid())
//                            && connections.getPk().getStart().getGid().equals(tracksLeft.get(i).getBusStop().getGid())
//                            && connections.getPk().getDirection().equals(1)) {
//                        //Check for <> and properitate diffrence
//                        if (tracksLeft.get(i + 1).getDeparture().getTime() > tracksLeft.get(i).getDeparture().getTime()) {
//                            connections.setTimeMili(tracksLeft.get(i + 1).getDeparture().getTime() - tracksLeft.get(i).getDeparture().getTime());
//                        } else {
//                            connections.setTimeMili(tracksLeft.get(i).getDeparture().getTime() - tracksLeft.get(i + 1).getDeparture().getTime());
//                        }
//                    }
//                    else {
//                        //insert
//                        params.put("start", connections.getPk().getStart().getGid());
//                        params.put("end", tracksLeft.get(i + 1).getBusStop().getGid());
//                        if (params.get("start").equals(params.get("end"))) continue;
//                        if (tracksLeft.get(i + 1).getDeparture().getTime() > tracksLeft.get(i).getDeparture().getTime()) {
//                            params.put("milis", tracksLeft.get(i + 1).getDeparture().getTime() - tracksLeft.get(i).getDeparture().getTime());
//                        } else {
//                            params.put("milis", tracksLeft.get(i).getDeparture().getTime() - tracksLeft.get(i + 1).getDeparture().getTime());
//                        }
//                        params.put("dir", 1);
//                        genericDao.runNativeQuery(sql3, params);
//                    }

//                }
//            }
//
//            if(!tracksRight.isEmpty())
//            for (int i = 0; i < tracksRight.size() - 1; ++i) {
//                Set<Connections> stopsFrom = tracksRight.get(i).getBusStop().getStopsFrom();
//                for (Connections connections : stopsFrom) {
//
//                    if (connections.getPk().getEnd().getGid().equals(tracksRight.get(i + 1).getBusStop().getGid())
//                            && connections.getPk().getStart().getGid().equals(tracksRight.get(i).getBusStop().getGid())
//                            && connections.getPk().getDirection().equals(2)) {
//                        //Check for <> and properitate diffrence
//                        if (tracksRight.get(i + 1).getDeparture().getTime() > tracksRight.get(i).getDeparture().getTime()) {
//                            connections.setTimeMili(tracksRight.get(i + 1).getDeparture().getTime() - tracksRight.get(i).getDeparture().getTime());
//                        } else {
//                            connections.setTimeMili(tracksRight.get(i).getDeparture().getTime() - tracksRight.get(i + 1).getDeparture().getTime());
//                        }
//                    }
//                    else {
//                        //insert
//                        params.put("start", connections.getPk().getStart().getGid());
//                        params.put("end", tracksRight.get(i + 1).getBusStop().getGid());
//                        if (tracksRight.get(i + 1).getDeparture().getTime() > tracksRight.get(i).getDeparture().getTime()) {
//                            params.put("milis", tracksRight.get(i + 1).getDeparture().getTime() - tracksRight.get(i).getDeparture().getTime());
//                        } else {
//                            params.put("milis", tracksRight.get(i).getDeparture().getTime() - tracksRight.get(i + 1).getDeparture().getTime());
//                        }
//                        params.put("dir", 2);
//                        genericDao.runNativeQuery(sql3, params);
//
//                    }

//                }
//            }


////            if (h == ((int) routesEntities.size() / 4) ) break;
//            Optional<TempLinksEntity> tempLinkL = links
//                    .stream()
//                    .filter(p -> p.getRoute().equals(routesEntity.getRouteName()) && p.getDirection().equals(1))
//                    .findFirst();
//            Optional<TempLinksEntity> tempLinkR = links
//                    .stream()
//                    .filter(p -> p.getRoute().equals(routesEntity.getRouteName()) && p.getDirection().equals(2))
//                    .findFirst();
//
//            Document docL = null;
//            Document docR = null;
//            Map<String, Object> params = new HashMap<>();
//            DateFormat formatter = new SimpleDateFormat("HH:mm");
//
//
//            List<TrackWrapper> tracksL = new LinkedList<>();
//            List<TrackWrapper> tracksR = new LinkedList<>();
//
//            Map<String, Object> par2 = new HashMap<>();
//            if (tempLinkL.isPresent()) {
//                docL = Jsoup.connect(tempLinkL.get().getUrl()).get();
//                Elements trl = docL.select("td:not(.strzalka)");
//                int j = 1;
//                for (int i = 0; i < trl.size() - 2; i += 2) {
//                    String time = trl.get(i + 1).child(0).text();
//                    String bus = trl.get(i).child(0).text().replace(" n/ż", "");
//                    if(bus.equals("Dańdówka Skrzyżowanie II")){
//                        bus = "Dańdówka Skrzyżowanie";
//                    }
//
//                    if(bus.equals("Dąbrówka Wielka DK 94")){
//                        bus = "Dąbrówka Wielka";
//                    }
//
//                    if(bus.equals("Hołdunów Przejazd Kolejowy (ul. Lędzińska)")){
//                        bus = "Hołdunów Przejazd Kolejowy";
//                    }
//
//                    Optional<Integer> busId = Optional.ofNullable(busStopDao.isExist(bus));
////                    if(!busId.isPresent()){
////                        par2.put("name", bus);
////                        par2.put("community_id", -1);
////                        genericDao.runNativeQuery(sql2, par2);
////                    }
////                tracksL.add(new TrackWrapper(j,busStopDao.isExist(bus), new Time(formatter.parse(time).getTime())));
//                    params.put("dir", 1);
//                    params.put("order", j);
//                    params.put("route", routesEntity.getRouteId());
//                    params.put("bus", busId.get());
//                    params.put("dep", new Time(formatter.parse(time).getTime()));
//                    genericDao.runNativeQuery(sql, params);
//
//                    ++j;
//                }
//            }
//
//            if (tempLinkR.isPresent()) {
//                docR = Jsoup.connect(tempLinkR.get().getUrl()).get();
//                Elements trr = docR.select("td:not(.strzalka)");
//
//                int j = 1;
//                for (int i = 0; i < trr.size() - 2; i += 2) {
//                    String time = trr.get(i + 1).child(0).text();
//                    String bus = trr.get(i).child(0).text().replace(" n/ż", "");
//                    if(bus.equals("Dańdówka Skrzyżowanie II")){
//                        bus = "Dańdówka Skrzyżowanie";
//                    }
//
//                    if(bus.equals("Dąbrówka Wielka DK 94")){
//                        bus = "Dąbrówka Wielka";
//                    }
//
//                    if(bus.equals("Hołdunów Przejazd Kolejowy (ul. Lędzińska)")){
//                        bus = "Hołdunów Przejazd Kolejowy";
//                    }
//                    Optional<Integer> busId = Optional.ofNullable(busStopDao.isExist(bus));
////                    if(!busId.isPresent()){
////                        par2.put("name", bus);
////                        par2.put("community_id", -1);
////                        genericDao.runNativeQuery(sql2, par2);
////                    }
////                tracksR.add(new TrackWrapper(j, busStopDao.isExist(bus), new Time(formatter.parse(time).getTime())));
//                    params.put("dir", 2);
//                    params.put("order", j);
//                    params.put("route", routesEntity.getRouteId());
//                    params.put("bus", busId.get());
//                    params.put("dep", new Time(formatter.parse(time).getTime()));
//                    genericDao.runNativeQuery(sql, params);
//                    ++j;
//                }
//            }
//
//
//            String bla = "";
//            ++h;
        }

    }

    @Transactional
    @SuppressWarnings("unchecked")
    public void parseAll() throws IOException, ParseException {
        final Map<Integer, String> stops = getStops();
        final List<String> files = getFilesNames();
        genericDao.senEntityClass(RoutesEntity.class);
        List<RoutesEntity> routesEntities = (List<RoutesEntity>) genericDao.getList();
        final Map<String, RoutesEntity> routes = routesEntities
                .stream()
                .collect(Collectors.toMap(RoutesEntity::getRouteName, p -> p));

        List<RouteWrapper> routesWrappers = new ArrayList<>();

        for (String file : files) {
            routesWrappers.add(parseFile(file, stops, routes));
        }

        final String queryTracks = "INSERT INTO geoportal.extended_tracks (stop_order, route_id, direction, bus_stop_id) " +
                "VALUES (:order, :route, :direction, :stop)";

        final String queryTime = "INSERT INTO geoportal.timetable " +
                "(departure, route_id, bus_stop_id, direction, description, annotation, service_id)" +
                "VALUES (:dep, :route, :stop, :dir, :desc, :anno, :service)";

        Map<String, Object> paramsTrack = new HashMap<>();
        Map<String, Object> paramsTime;

        for (RouteWrapper routeWrapper : routesWrappers) {
            int order = 1;
            for (BusStopWrapper busStopWrapper : routeWrapper.getTrack()) {

                //Update extended tracks
                paramsTrack.put("order", order);
                paramsTrack.put("route", routeWrapper.getRouteId());
                paramsTrack.put("direction", routeWrapper.getDirection());
                paramsTrack.put("stop", busStopWrapper.getBusStopId());

                genericDao.runNativeQuery(queryTracks, paramsTrack);

                for (TimeWrapper timeWrapper : busStopWrapper.getWorkingDays()) {
                    paramsTime = prepareParameters(routeWrapper, busStopWrapper, timeWrapper, 1);
                    genericDao.runNativeQuery(queryTime, paramsTime);
                }
                for (TimeWrapper timeWrapper : busStopWrapper.getSaturdays()) {
                    paramsTime = prepareParameters(routeWrapper, busStopWrapper, timeWrapper, 2);
                    genericDao.runNativeQuery(queryTime, paramsTime);
                }
                for (TimeWrapper timeWrapper : busStopWrapper.getSundays()) {
                    paramsTime = prepareParameters(routeWrapper, busStopWrapper, timeWrapper, 3);
                    genericDao.runNativeQuery(queryTime, paramsTime);
                }

                ++order;
            }
        }
    }

    private Map<String, Object> prepareParameters(RouteWrapper routeWrapper, BusStopWrapper busStopWrapper,
                                                  TimeWrapper timeWrapper, int service) {
        Map<String, Object> params = new HashMap<>();
        params.put("dep", timeWrapper.getTime());
        params.put("route", routeWrapper.getRouteId());
        params.put("stop", busStopWrapper.getBusStopId());
        params.put("desc", routeWrapper.getName());
        params.put("service", service);
        params.put("dir", routeWrapper.getDirection());
        Optional<String> anno = Optional.ofNullable(timeWrapper.getAnnotation());
        if (anno.isPresent()) {
            params.put("anno", timeWrapper.getAnnotation());
        } else {
            params.put("anno", "");
        }
        return params;
    }

    @Transactional
    public void addAnnotation() throws IOException {
        @Cleanup InputStream in = getClass().getResourceAsStream("/6/adnotacje.txt");
        @Cleanup BufferedReader br = new BufferedReader(new InputStreamReader(in, charset));

        final String query = "INSERT INTO geoportal.info (parsing_alias, real_alias, long_description)" +
                "VALUES (:parsing_alias, :real_alias, :long_description)";

        Map<String, Object> params = new HashMap<>();
        String[] splitted;

        String line;
        while ((line = br.readLine()) != null) {
            line = line.replaceFirst(" ", ";");
            line = line.replaceFirst(" ", ";");
            splitted = line.split(";");
            params.put("parsing_alias", splitted[0]);
            params.put("real_alias", splitted[1]);
            params.put("long_description", splitted[2]);
            genericDao.runNativeQuery(query, params);
        }

    }

    private Map<Integer, String> getStops() throws IOException {
        @Cleanup InputStream in = getClass().getResourceAsStream("/6/przystanki.txt");
        @Cleanup BufferedReader br = new BufferedReader(new InputStreamReader(in, charset));

        final Map<Integer, String> stops = new HashMap<>();

        String[] splitted;
        //Get stops
        String line;
        while ((line = br.readLine()) != null) {
            line = line.replaceFirst(" ", ";");
            splitted = line.split(";");
            stops.put(Integer.parseInt(splitted[0]), splitted[1]);
        }

        return stops;
    }

    private List<String> getFilesNames() throws IOException {
        @Cleanup InputStream in = getClass().getResourceAsStream("/6/linie_smaller.txt");
        @Cleanup BufferedReader br = new BufferedReader(new InputStreamReader(in, charset));
        String line;
        List<String> files = new ArrayList<>();
        while ((line = br.readLine()) != null) {
            files.add(line);
        }
        return files;
    }

    private RouteWrapper parseFile(String file, Map<Integer, String> stops, Map<String, RoutesEntity> routes) throws IOException, ParseException {

        @Cleanup InputStream in = getClass().getResourceAsStream("/6/" + file);
        @Cleanup BufferedReader br = new BufferedReader(new InputStreamReader(in, charset));

        RouteWrapper routeWrapper = new RouteWrapper();
        List<BusStopWrapper> busStopWrappers = new LinkedList<>();

        String line;
        RoutesEntity route;

        int counter = 1;
        int type = 0;

        Matcher m1, m2;

        BusStopWrapper busStop = new BusStopWrapper();
        List<TimeWrapper> timeN = new ArrayList<>();
        List<TimeWrapper> timeSat = new ArrayList<>();
        List<TimeWrapper> timeSun = new ArrayList<>();

        for (; (line = br.readLine()) != null; ++counter) {

            //Setting line info
            if (counter == 1) {
                route = routes.get(line);
                routeWrapper.setName(line);
                routeWrapper.setRouteId(route.getRouteId());
                continue;
            } else if (counter <= 3 || line.equals(SKIP)) {
                continue;
            }

            //Real parsing, track, bus stops and times
            if (counter % 4 == 0) {
                busStop = new BusStopWrapper();
                timeN = new ArrayList<>();
                timeSat = new ArrayList<>();
                timeSun = new ArrayList<>();
                busStop.setBusStopId(busStopDao.isExist(stops.get(Integer.parseInt(line))));
                busStopWrappers.add(busStop);
                continue;
            }

            if (counter % 4 == 1) {
                type = 1; //PON - PT
            } else if (counter % 4 == 2) {
                type = 2; //SOB
            } else if (counter % 4 == 3) {
                type = 3; //ND
            }


            if (line.equals(CLONE)) {
                if (type == 2) {
                    timeSat.addAll(busStop.getWorkingDays());
                } else if (type == 3) {
                    timeSun.addAll(busStop.getSaturdays());
                }
                continue;
            }

            String[] times = line.split(",");
            String founded;
            for (String tm : times) {
                m1 = p1.matcher(tm);
                m2 = p2.matcher(tm);

                //4 numbers
                TimeWrapper timeWrapper = new TimeWrapper();

                if (m2.find()) {
                    founded = m2.group();
                    if (founded.length() > 4) {
                        timeWrapper.setTime(new Time(sdf.parse(founded.substring(0, 4)).getTime()));
                        timeWrapper.setAnnotation(founded.substring(4, 6));
                    } else {
                        timeWrapper.setTime(new Time(sdf.parse(founded).getTime()));
                    }
                    //3 numbers
                } else if (m1.find()) {
                    founded = m1.group();
                    if (founded.length() > 3) {
                        timeWrapper.setTime(new Time(sdf.parse("0" + founded.substring(0, 3)).getTime()));
                        timeWrapper.setAnnotation(founded.substring(3, 5));
                    } else {
                        timeWrapper.setTime(new Time(sdf.parse("0" + founded).getTime()));
                    }
                }

                if (type == 1) {
                    timeN.add(timeWrapper);
                } else if (type == 2) {
                    timeSat.add(timeWrapper);
                } else if (type == 3) {
                    timeSun.add(timeWrapper);
                }
            }

            busStop.setWorkingDays(timeN);
            busStop.setSaturdays(timeSat);
            busStop.setSundays(timeSun);
        }

        routeWrapper.setTrack(busStopWrappers);
        if (file.contains("-0.txt")) {
            routeWrapper.setDirection(1);
        } else {
            routeWrapper.setDirection(2);
        }

        return routeWrapper;
    }

}
