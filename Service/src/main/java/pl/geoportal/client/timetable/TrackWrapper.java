package pl.geoportal.client.timetable;

import lombok.Data;

import java.sql.Time;

@Data
public class TrackWrapper {
    private final int order;
    private final int busStop;
    private final Time time;
}
