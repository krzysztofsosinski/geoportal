package pl.geoportal.client;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class BusStopDTO {

    @JsonProperty("nazwa_przystanku")
    public String name;
    public String text;
    public double lat;
    public double lng;
    @JsonProperty("id_gminy")
    public int gid;
    public Integer id;
    @JsonProperty("ikona")
    public int type;

}
