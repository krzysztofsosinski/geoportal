package pl.geoportal.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.geoportal.model.UserEntity;
import pl.geoportal.persistence.UserDao;

import javax.transaction.Transactional;

@Service
public class UserService {

    @Autowired
    private UserDao userDao;

    @Transactional
    public UserEntity findByEmail(String email) {
        return userDao.findByEmail(email);
    }

}
