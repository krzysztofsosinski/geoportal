package pl.geoportal.db;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.PrecisionModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.geoportal.client.BusStopDTO;
import pl.geoportal.model.BusStopsEntity;
import pl.geoportal.persistence.BusStopsDao;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class BusStopService {

    @Autowired
    BusStopsDao busStopsDao;

    @Transactional
    public void saveBusStop(BusStopDTO busStopDTO){
        if(busStopDTO.name.equals("")) return;
        BusStopsEntity busStopsEntity = mapDTOToEntity(busStopDTO);
        BusStopsEntity objFromDb = busStopsDao.checkIfPointExists(busStopsEntity.getGeom());
        if(objFromDb != null){
            objFromDb = updateEntity(objFromDb, busStopsEntity);
            busStopsDao.save(objFromDb);
        }else{
            busStopsDao.save(busStopsEntity);
        }
    }

    @Transactional
    public void updateBusStop(BusStopDTO busStopDTO){
        final GeometryFactory factory = new GeometryFactory(new PrecisionModel(), 4326);
        String name = busStopDTO.name.replace(" n/�", "");
        BusStopsEntity busStopsEntity = busStopsDao.getByName(name);
        if(busStopsEntity != null){
            busStopsEntity.setCommunityId(busStopDTO.gid);
            busStopsEntity.setGeom(factory.createPoint(new Coordinate(busStopDTO.lng, busStopDTO.lat)));
        }
    }

    @Transactional(readOnly = true)
    public List<BusStopDTO> getBusNames(String searchNeedle, Integer maxElements){
        return busStopsDao.searchByName(searchNeedle, maxElements)
                .stream()
                .map(this::mapEntityToDTO)
                .collect(Collectors.toList());
    }

    private BusStopsEntity mapDTOToEntity(BusStopDTO busStopDTO){
        final GeometryFactory factory = new GeometryFactory(new PrecisionModel(), 4326);
        final BusStopsEntity busStopsEntity = new BusStopsEntity();
        busStopsEntity.setName(busStopDTO.name);
        busStopsEntity.setCommunityId(busStopDTO.gid);
        busStopsEntity.setGeom(factory.createPoint(new Coordinate(busStopDTO.lat, busStopDTO.lng)));
        busStopsEntity.setStopTypeByType(busStopsDao.getType(busStopDTO.type));
        return busStopsEntity;
    }

    private BusStopDTO mapEntityToDTO(BusStopsEntity busStopsEntity){
        BusStopDTO busStopDTO = new BusStopDTO();
        busStopDTO.name = busStopsEntity.getName();
        busStopDTO.text = busStopsEntity.getName();
        busStopDTO.id = busStopsEntity.getGid();
        return busStopDTO;
    }

    private BusStopsEntity updateEntity(BusStopsEntity busStopsEntity, BusStopsEntity updateData){
        busStopsEntity.setGeom(updateData.getGeom());
        busStopsEntity.setName(updateData.getName());
        busStopsEntity.setStopTypeByType(busStopsDao.getType(updateData.getStopTypeByType().getId()));
        busStopsEntity.setCommunityId(updateData.getCommunityId());
        return busStopsEntity;
    }

}
