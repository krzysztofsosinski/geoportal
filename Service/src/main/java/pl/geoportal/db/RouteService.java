package pl.geoportal.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.geoportal.client.RouteDTO;
import pl.geoportal.model.RoutesEntity;
import pl.geoportal.persistence.RoutesDao;

import java.util.ArrayList;
import java.util.List;

@Service
public class RouteService {

    @Autowired
    RoutesDao routesDao;

    @Transactional(readOnly = true)
    public List<RouteDTO> searchRoutes(String needle, Integer max){
        List<RouteDTO> result = new ArrayList<>();
        for(RoutesEntity routesEntity : routesDao.searchByName(needle, max)){
            RouteDTO routeDTO = new RouteDTO();
            routeDTO.id = routesEntity.getRouteId();
            routeDTO.text = routesEntity.getRouteName();
            result.add(routeDTO);
        }
        return result;
    }
}
