package pl.geoportal.logic;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Vertex{

    public final String name;
    public final int id;
    public final int gid;
    public List<Edge> adjacencies = new ArrayList<>();
    public double wage = Double.POSITIVE_INFINITY;

    public Vertex previous;

    public long choosenTime;
    public Integer route;
    public int stops = 0;

    //Needed for SSP
    public double fz = Double.POSITIVE_INFINITY;
    public double time = Double.POSITIVE_INFINITY;
    public double cost = Double.POSITIVE_INFINITY;
    public double deltaCost = Double.POSITIVE_INFINITY;

    public Vertex(String name, Integer id, Integer gid) {
        adjacencies = new ArrayList<>();
        this.name = name;
        this.id = id;
        this.gid = gid;
    }

    public Vertex(Vertex vertex) {
        this.name = vertex.name;
        this.id = vertex.id;
        this.gid = vertex.gid;
    }

    public String toString() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Vertex vertex = (Vertex) o;
        return Objects.equals(id, vertex.id) &&
                Objects.equals(gid, vertex.gid) &&
                Objects.equals(name, vertex.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, id, gid);
    }
}
