package pl.geoportal.logic.Graph;

import pl.geoportal.model.Connections;
import pl.geoportal.model.ExtConnections;

import java.util.Objects;

public class SimplifiedConnection{

    public int start;
    public int end;
    public Double time;
    public Double distance;
    public Long timeMs;
    public Boolean isExtConnection = Boolean.FALSE;
    public int route_id = -1;

    public SimplifiedConnection(int start, int end, Double time, Double distance, Long timeMs) {
        this.start = start;
        this.end = end;
        this.time = time;
        this.distance = distance;
        this.timeMs = timeMs;
    }


    public SimplifiedConnection(Connections connections){
        this(connections.getPk().getStart().getGid(),
                connections.getPk().getEnd().getGid(),
                connections.getTime(), connections.getDistance(), connections.getTimeMili());
    }

    public SimplifiedConnection(ExtConnections extConnections){
        this.start = extConnections.getPk().getStart().getGid();
        this.end = extConnections.getPk().getEnd().getGid();
        this.isExtConnection = Boolean.TRUE;
        this.route_id = extConnections.getPk().getRoute().getRouteId();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SimplifiedConnection that = (SimplifiedConnection) o;
        return Objects.equals(start, that.start) &&
                Objects.equals(end, that.end) &&
                Objects.equals(route_id, that.route_id) &&
                Objects.equals(time, that.time) &&
                Objects.equals(distance, that.distance) &&
                Objects.equals(timeMs, that.timeMs) &&
                Objects.equals(isExtConnection, that.isExtConnection);
    }

    @Override
    public int hashCode() {
        return Objects.hash(start, end, time, distance, timeMs, isExtConnection, route_id);
    }
}
