package pl.geoportal.logic.Graph;

import pl.geoportal.model.TimetableEntity;

import java.sql.Time;

public class SimplifiedTimetable {

    public int id;
    public int routeId;
    public int busId;
    public Time time;
    public String name;
    public SimplifiedRoute route;

    public SimplifiedTimetable(int id, int routeId, int busId, Time time) {
        this.id = id;
        this.routeId = routeId;
        this.busId = busId;
        this.time = time;
    }

    public SimplifiedTimetable(TimetableEntity timetableEntity){
        this(timetableEntity.getId(),
                timetableEntity.getRoute().getRouteId(),
                timetableEntity.getBusStop().getGid(),
                timetableEntity.getDeparture()
                );
        this.route = new SimplifiedRoute(timetableEntity.getRoute());
    }
}
