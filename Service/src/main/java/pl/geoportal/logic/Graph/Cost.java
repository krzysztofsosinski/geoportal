package pl.geoportal.logic.Graph;

public class Cost {

    public final Double cost;
    public final Boolean transfer;
    public final Integer route;
    public final Boolean startFromC2;
    public final Boolean changingRegion;

    public Cost(Double cost, Boolean transfer, Integer route, Boolean startFromC2, Boolean changingRegion) {
        this.cost = cost;
        this.transfer = transfer;
        this.route = route;
        this.startFromC2 = startFromC2;
        this.changingRegion = changingRegion;
    }

}
