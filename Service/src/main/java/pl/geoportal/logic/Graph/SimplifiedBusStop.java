package pl.geoportal.logic.Graph;

import java.util.List;
import java.util.Set;

public class SimplifiedBusStop {

    public int gid;
    public String name;
    public Integer communityId;
    public int stopType;
    public List<SimplifiedTimetable> times;
    public Set<SimplifiedConnection> stopsFrom;
    public Set<SimplifiedConnection> stopsTo;

    public SimplifiedBusStop(int gid, String name, Integer communityId, int stopType, List<SimplifiedTimetable> times,
                             Set<SimplifiedConnection> stopsFrom, Set<SimplifiedConnection> stopsTo) {
        this.gid = gid;
        this.name = name;
        this.communityId = communityId;
        this.stopType = stopType;
        this.times = times;
        this.stopsFrom = stopsFrom;
        this.stopsTo = stopsTo;
    }
}
