package pl.geoportal.logic.Graph;

import pl.geoportal.model.RoutesEntity;

public class SimplifiedRoute {


    public int id;
    public String name;
    private short routeType;

    public SimplifiedRoute(int id, String name, short routeType) {
        this.id = id;
        this.name = name;
        this.routeType = routeType;
    }

    public SimplifiedRoute(RoutesEntity routesEntity){
        this(routesEntity.getRouteId(),
                routesEntity.getRouteName(),
                routesEntity.getRouteType());
    }
}
