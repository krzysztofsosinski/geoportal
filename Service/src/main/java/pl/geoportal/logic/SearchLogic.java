package pl.geoportal.logic;

import com.google.common.collect.Sets;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;
import pl.geoportal.dto.ResponseDTO;
import pl.geoportal.logic.Graph.SimplifiedBusStop;
import pl.geoportal.logic.Graph.SimplifiedConnection;
import pl.geoportal.logic.Graph.SimplifiedTimetable;
import pl.geoportal.logic.Keys.TrackKey;
import pl.geoportal.model.BusStopsEntity;
import pl.geoportal.persistence.DTO.SimplifiedRoutesTrack;
import pl.geoportal.persistence.DTO.SimplifiedTimeTable;
import pl.geoportal.persistence.DTO.TimetableKey;
import pl.geoportal.persistence.GenericDao;
import pl.geoportal.persistence.RoutesTracksDao;
import pl.geoportal.persistence.TimeTableDao;

import javax.annotation.PostConstruct;
import java.text.ParseException;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class SearchLogic {

    @Autowired
    @Qualifier("transactionManager")
    protected PlatformTransactionManager txManager;

    @Autowired
    Dijkstra dijkstra;

    @Autowired
    TimeTableDao timeTableDao;

    @Autowired
    RoutesTracksDao routesTracksDao;

    @Autowired
    private GenericDao<BusStopsEntity, Integer> genericDao;

    //Immutable collections
    private final static Map<Integer, SimplifiedBusStop> GRAPH_MAP = new ConcurrentHashMap<>();
    private final static Map<TimetableKey, List<SimplifiedTimeTable>> TIMETABLES = new ConcurrentHashMap<>();
    private final static Set<TrackKey> TRACKS = Sets.newConcurrentHashSet();
    private final static Map<Integer, Vertex> INITIAL_GRAPH = new ConcurrentHashMap<>();

    private final static Function<Map.Entry<Integer, SimplifiedBusStop>, Vertex> BUS_STOP_TO_VERTEX_TIME = integerBusStopsEntityEntry -> new Vertex(
            integerBusStopsEntityEntry.getValue().name,
            integerBusStopsEntityEntry.getKey(),
            integerBusStopsEntityEntry.getValue().communityId
    );

    private final static Function<BusStopsEntity, SimplifiedBusStop> SIMPLIFIED_BUS_STOPS = busStopsEntity -> {
        Set<SimplifiedConnection> from = busStopsEntity.getStopsFrom().stream()
                .map(SimplifiedConnection::new).collect(Collectors.toSet());

//        Set<SimplifiedConnection> to = busStopsEntity.getStopsTo().stream()
//                .map(SimplifiedConnection::new).collect(Collectors.toSet());

        from.addAll(busStopsEntity.getExtStopsFrom().stream().map(SimplifiedConnection::new).collect(Collectors.toSet()));
//        to.addAll(busStopsEntity.getExtStopsTO().stream().map(SimplifiedConnection::new).collect(Collectors.toSet()));

        List<SimplifiedTimetable> times = new ArrayList<>();
        int type = busStopsEntity.getStopTypeByType().getId();
        return new SimplifiedBusStop(
                busStopsEntity.getGid(),
                busStopsEntity.getName(),
                busStopsEntity.getCommunityId(),
                type, times, from, null
        );
    };

    private final Function<Vertex, ResponseDTO> PREPARE_RESPONSE = vertex -> new ResponseDTO(vertex.name, vertex.id);

    @PostConstruct
    private void init() {
        TransactionTemplate tmpl = new TransactionTemplate(txManager);
        tmpl.setReadOnly(true);
        tmpl.execute(new TransactionCallbackWithoutResult() {
            @Override
            protected void doInTransactionWithoutResult(TransactionStatus status) {
                GRAPH_MAP.putAll(
                        genericDao.getAll(BusStopsEntity.class)
                                .stream()
                                .map(SIMPLIFIED_BUS_STOPS)
                                .collect(Collectors.toMap(p -> p.gid, p -> p))
                );
                TIMETABLES.putAll(timeTableDao.getAll());
                TRACKS.addAll(prepareTrackKeys());
                INITIAL_GRAPH.putAll(prepareInitialGraph(GRAPH_MAP, BUS_STOP_TO_VERTEX_TIME));
            }
        });
    }

    private Set<TrackKey> prepareTrackKeys() {
        final List<SimplifiedRoutesTrack> tracks = routesTracksDao.getTracks();
        List<SimplifiedRoutesTrack> left = new ArrayList<>();
        List<SimplifiedRoutesTrack> right = new ArrayList<>();
        Set<Integer> routeId = new HashSet<>();
        tracks.forEach(
                p -> {
                    if (p.getType() == 1) {
                        routeId.add(p.getRouteId());
                        left.add(p);
                    } else {
                        routeId.add(p.getRouteId());
                        right.add(p);
                    }
                }
        );
        Map<Integer, List<SimplifiedRoutesTrack>> routesTracksLeft = new HashMap<>();
        Map<Integer, List<SimplifiedRoutesTrack>> routesTracksRight = new HashMap<>();

        for (Integer id : routeId) {
            routesTracksLeft.put(id,
                    left
                            .stream()
                            .filter(p -> p.getRouteId().equals(id))
                            .sorted((o1, o2) -> o1.getOrderNumber().compareTo(o2.getOrderNumber()))
                            .collect(Collectors.toList())
            );
            routesTracksRight.put(id,
                    right
                            .stream()
                            .filter(p -> p.getRouteId().equals(id))
                            .sorted((o1, o2) -> o1.getOrderNumber().compareTo(o2.getOrderNumber()))
                            .collect(Collectors.toList())
            );
        }

        final Set<TrackKey> trackKeySet = new HashSet<>();

        routesTracksLeft.entrySet().forEach(p -> {

                    for (int i = 0; i < p.getValue().size() - 1; ++i) {
                        trackKeySet.add(new TrackKey(
                                        p.getValue().get(i).getBusStopGid(),
                                        p.getValue().get(i + 1).getBusStopGid(),
                                        1,
                                        p.getKey()
                                )
                        );
                    }
                }
        );

        routesTracksRight.entrySet().forEach(p -> {

                    for (int i = 0; i < p.getValue().size() - 1; ++i) {
                        trackKeySet.add(new TrackKey(
                                        p.getValue().get(i).getBusStopGid(),
                                        p.getValue().get(i + 1).getBusStopGid(),
                                        2,
                                        p.getKey()
                                )
                        );
                    }
                }
        );

        return trackKeySet;
    }

    //Imielin Wiadukt - 3229
    //Katowice Korfantego - 1925
    public List<List<ResponseDTO>> shortestPath(int start, int end, String startTime) throws ParseException {
        long measureTime = System.currentTimeMillis();
        Double tMin = 0.0;
        Double cMin = 0.0;

        List<List<Vertex>> result = new ArrayList<>();

        //Create time network and compute best path
        Map<Integer, Vertex> graphTime = resetGraph(INITIAL_GRAPH);
        dijkstra.computePathsTime(graphTime.get(start), startTime, TIMETABLES, TRACKS, end);
        List<Vertex> minTime = dijkstra.getShortestPathTo(graphTime.get(end));
        result.add(minTime);
        tMin = minTime.get(minTime.size()-1).wage;

        //Create cost network and compute best path
        Map<Integer, Vertex> graphCost = resetGraph(INITIAL_GRAPH);
        dijkstra.computePathsCost(graphCost.get(start), end);
        List<Vertex> minCost = dijkstra.getShortestPathToByCost(graphCost.get(end));
        result.add(minCost);
        cMin = minCost.get(minCost.size()-1).wage;

        result.addAll(ssp(
                cMin, tMin,
                dijkstra.countMaxTimeFromCostPath(minCost, TIMETABLES, TRACKS, startTime),
                dijkstra.countMaxCostFromTimePath(minTime, null),
                minTime
        ));

        //result
        List<List<ResponseDTO>> responseDTOs = new ArrayList<>();
        result.stream().forEach(p -> responseDTOs.add(p.stream().map(PREPARE_RESPONSE).collect(Collectors.toList())));

        long stopTime = System.currentTimeMillis();
        long elapsedTime = stopTime - measureTime;
        System.out.println("Counting min time : " + elapsedTime);
        return responseDTOs;
    }

    private class StackPath {
        public List<Vertex> path;
        public int i;

        public StackPath(List<Vertex> path, int i) {
            this.path = path;
            this.i = i;
        }
    }

    private class IgnoredEdge {
        public int start;
        public int end;

        public IgnoredEdge(int start, int end) {
            this.start = start;
            this.end = end;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            IgnoredEdge that = (IgnoredEdge) o;
            return java.util.Objects.equals(start, that.start) &&
                    java.util.Objects.equals(end, that.end);
        }

        @Override
        public int hashCode() {
            return java.util.Objects.hash(start, end);
        }
    }

    enum States {
        CREATE_PATH,
        NEW_PATH,
        NO_PATH,
    }

    private List<List<Vertex>> ssp(
            double minCost, double minTime,
            double maxTime, double maxCost,
            List<Vertex> pAkt) {

        States state = States.CREATE_PATH;

        List<List<Vertex>> results = new ArrayList<>();
        Stack<StackPath> paths = new Stack<>();
        Set<IgnoredEdge> ignoredEdges = new HashSet<>();

        Set<Integer> ignoredW = new HashSet<>();
        List<Vertex> newPAkt = new ArrayList<>();

        double deltaTime;
        double deltaCost;

        int i = 0;
        boolean stop = Boolean.FALSE;

        if (minCost == minTime) {
            deltaTime = 0.5;
            deltaCost = 0.5;
        } else {
            deltaTime = (maxCost - minCost) / (maxCost - minCost + maxTime - minTime);
            deltaCost = (maxTime - minTime) / (maxCost - minCost + maxTime - minTime);
        }

        double criterionMax = deltaTime * maxTime + deltaCost * maxCost;

        pAkt.get(0).cost = 0.0;
        pAkt.get(0).time = 0.0;
        pAkt.get(0).stops = 0;

        while (!stop) {
            while (i < pAkt.size() - 1 && !state.equals(States.NEW_PATH)) {
                ignoredEdges.add(new IgnoredEdge(pAkt.get(i).id, pAkt.get(i + 1).id));
                Map<Integer, Vertex> newGraph = resetGraphWithIgnoredEdgesAndW(INITIAL_GRAPH, ignoredEdges, ignoredW);

                newPAkt = new ArrayList<>();

                for(Vertex vr : pAkt){
                    if(vr.id == pAkt.get(i).id){
                        Vertex vertex = newGraph.get(vr.id);
                        vertex.cost =  vr.cost;
                        vertex.time = vr.time;
                        vertex.previous = (vr.previous != null) ? vr.previous: null;
                        vertex.stops = vr.stops;
                        vertex.route = vr.route;
                        newPAkt.add(vertex);
                        break;
                    }else{
                        newPAkt.add(vr);
                    }
                }

                newPAkt = dijkstra.createNewKPath(
                        newPAkt, deltaTime, deltaCost, criterionMax, newPAkt.get(i),
                        newGraph.get(pAkt.get(pAkt.size() - 1).id),
                        newGraph,
                        TIMETABLES, TRACKS
                );

                if (!newPAkt.isEmpty()) {
                    state = States.NEW_PATH;
                } else {
                    state = States.NO_PATH;
                }

                if (state.equals(States.NO_PATH)) {
                    ignoredW.add(pAkt.get(i).id);
                    ++i;
                }
            }
            if (state.equals(States.NEW_PATH)) {
                results.add(newPAkt);
                paths.push(new StackPath(pAkt, i));
                pAkt = newPAkt;
                for (int j = 0; j < newPAkt.size(); ++j) {
                    if (pAkt.size() - 1 == j) {
                        i = j;
                        break;
                    }
                    if (newPAkt.get(j).id != pAkt.get(j).id) {
                        i = j - 1;
                        break;
                    }
                }
            } else {
                for (int k = 0; k < pAkt.size() - 1; ++k) {
                    Integer start = pAkt.get(k).id;
                    Integer end = pAkt.get(k + 1).id;
                    if (ignoredW.contains(start)) {
                        ignoredW.remove(start);
                    } else if (ignoredW.contains(end)) {
                        ignoredW.remove(end);
                    }
                    IgnoredEdge ignoredEdge = new IgnoredEdge(start, end);
                    if (ignoredEdges.contains(ignoredEdge)) {
                        ignoredEdges.remove(ignoredEdge);
                    }
                }
                if (!paths.isEmpty()) {
                    StackPath stackPath = paths.pop();
                    pAkt = stackPath.path;
                    i = stackPath.i;
                    ignoredW.add(pAkt.get(i).id);
                    ++i;
                } else {
                    stop = Boolean.TRUE;
                }
            }
        }

        return results;
    }


    private Map<Integer, Vertex> resetGraph(Map<Integer, Vertex> grph) {
        Map<Integer, Vertex> vertexes = new HashMap<>();
        for (Vertex entry : grph.values()) {
            Vertex temp = new Vertex(entry);
            vertexes.put(
                    temp.id,
                    temp
            );
        }

        for (Vertex entry : grph.values()) {
            Vertex parent = vertexes.get(entry.id);
            parent.adjacencies.addAll(entry.adjacencies.stream().map(edge -> new Edge(
                    vertexes.get(edge.target.id),
                    edge.weight,
                    edge.isExt,
                    edge.route
            )).collect(Collectors.toList()));
        }

        return vertexes;
    }

    private Map<Integer, Vertex> resetGraphWithIgnoredEdgesAndW(Map<Integer, Vertex> grph,
                                                                Set<IgnoredEdge> ignoredEdges, Set<Integer> ignoredW
    ) {
        Map<Integer, Vertex> vertexes = new HashMap<>();
        for (Vertex entry : grph.values()) {
            if (ignoredW.contains(entry.id)) continue;
            Vertex temp = new Vertex(entry);
            vertexes.put(
                    temp.id,
                    temp
            );
        }

        for (Vertex entry : grph.values()) {
            if (ignoredW.contains(entry.id)) continue;
            Vertex parent = vertexes.get(entry.id);
            parent.adjacencies.addAll(entry.adjacencies
                    .stream()
                    .filter(edg -> !ignoredW.contains(edg.target.id) && !ignoredEdges.contains(new IgnoredEdge(parent.id, edg.target.id)))
                    .map(edge -> new Edge(
                            vertexes.get(edge.target.id),
                            edge.weight,
                            edge.isExt,
                            edge.route
                    )).collect(Collectors.toList()));
        }

        return vertexes;
    }


    private Map<Integer, Vertex> prepareInitialGraph(Map<Integer, SimplifiedBusStop> graph,
                                                     Function<Map.Entry<Integer, SimplifiedBusStop>, Vertex> mapper) {

        Map<Integer, Vertex> vertexes = new HashMap<>();

        for (Map.Entry<Integer, SimplifiedBusStop> entry : graph.entrySet()) {
            Vertex vertex = mapper.apply(entry);
            vertexes.put(vertex.id, vertex);
        }

        for (Vertex vertexE : vertexes.values()) {
            for (SimplifiedConnection connection : graph.get(vertexE.id).stopsFrom) {
                if (!connection.isExtConnection) {
                        final Long weightTime = (connection.timeMs != null) ? connection.timeMs: Long.MAX_VALUE;
                        List<Integer> routes = TRACKS
                                .stream()
                                .filter(trackKey -> trackKey.startId == vertexE.id && trackKey.endId == connection.end)
                                .map(trackKey -> trackKey.routeId)
                                .collect(Collectors.toList());

                        vertexE.adjacencies.addAll(routes.stream().map(integer -> new Edge(
                                vertexes.get(connection.end),
                                weightTime,
                                integer
                        )).collect(Collectors.toList()));
                } else {
                    vertexE.adjacencies.add(
                            new Edge(
                                    vertexes.get(connection.end),
                                    Long.MAX_VALUE,
                                    true,
                                    connection.route_id
                            )
                    );
                }
            }
        }
        return vertexes;
    }

//    private Map<Integer, Vertex> prepareGraphTime(Map<Integer, SimplifiedBusStop> graph,
//                                                  Function<Map.Entry<Integer, SimplifiedBusStop>, Vertex> mapper,
//                                                  GraphType type) {
//
//        Map<Integer, Vertex> vertexes = new HashMap<>();
//
//        for (Map.Entry<Integer, SimplifiedBusStop> entry : graph.entrySet()) {
//            Vertex vertex = mapper.apply(entry);
//            vertexes.put(vertex.id, vertex);
//        }
//
//        //Adding connections
//        vertexes
//                .entrySet()
//                .stream().forEach(p -> graph.get(p.getKey()).stopsFrom
//                .stream().forEach(
//                        k -> {
//                            if (type.equals(GraphType.DISTANCE)) {
//                                if (k.distance != null)
//                                    p.getValue().adjacencies.add(
//                                            new Edge(vertexes.get(k.end),
//                                                    k.distance,
//                                                    Long.MIN_VALUE)
//                                    );
//                            } else if (type.equals(GraphType.COST)) {
//                                p.getValue().adjacencies.add(
//                                        new Edge(vertexes.get(k.end),
//                                                k.distance == null ? Double.POSITIVE_INFINITY : k.distance,
//                                                Long.MIN_VALUE)
//                                );
//                            }
//                        }
//                ));
//        return vertexes;
//    }

    private enum GraphType {
        DISTANCE,
        COST,
    }

}
