package pl.geoportal.logic;


import java.util.*;

public class BreadthFirstSearch {

    private Set<Integer> checked;
    private Map<Integer, Integer> conntectedTo;

    private final int SOURCE;
    private final Map<Integer, Vertex> GRAPH;

    public BreadthFirstSearch(Map<Integer, Vertex> graph, int startPoint){
        SOURCE = startPoint;
        GRAPH = graph;
        checked = new HashSet<>();
        conntectedTo = new HashMap<>();
        makeBfs(graph, startPoint);
    }

    private void makeBfs(Map<Integer, Vertex> graph, int source){
        Queue<Vertex> parents = new ArrayDeque<>();
        Vertex sourceVertex = graph.get(source);
        this.checked.add(sourceVertex.id);
        parents.add(sourceVertex);

        while(!parents.isEmpty()){
            Vertex parent = parents.remove();
            for(Edge e : parent.adjacencies ){
                if(!checked.contains(e.target.id)){
                    conntectedTo.put(e.target.id, parent.id);
                    checked.add(e.target.id);
                    parents.add(e.target);
                }
            }
        }
    }

    public Optional<List<Vertex>> pathTo(Vertex target){
        if(!checked.contains(target.id))
            return Optional.empty();

        List<Vertex> path = new ArrayList<>();
        path.add(target);

        while(conntectedTo.get(target.id) != SOURCE){
            target = GRAPH.get(conntectedTo.get(target.id));
            path.add(target);
        }
        path.add(GRAPH.get(SOURCE));

        Collections.reverse(path);
        return Optional.of(path);
    }
}
