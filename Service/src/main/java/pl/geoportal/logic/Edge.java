package pl.geoportal.logic;

import java.util.Objects;

public class Edge{
    public final Vertex target;

    public final Long weight;
    public Boolean isExt = Boolean.FALSE;
    public final Integer route;

    public Edge(Vertex target, final Long weight, final Integer route) {
        this.target = target;
        this.weight = weight;
        this.route = route;
    }

    public Edge(Vertex target, Long weight, Boolean isExt, final Integer route) {
        this(target, weight, route);
        this.isExt = isExt;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Edge edge = (Edge) o;
        return Objects.equals(target, edge.target) &&
                Objects.equals(weight, edge.weight) &&
                Objects.equals(isExt, edge.isExt) &&
                Objects.equals(route, edge.route);
    }

    @Override
    public int hashCode() {
        return Objects.hash(target, weight, isExt, route);
    }
}
