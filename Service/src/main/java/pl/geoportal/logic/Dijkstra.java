package pl.geoportal.logic;

import org.springframework.stereotype.Component;
import pl.geoportal.logic.Graph.Cost;
import pl.geoportal.logic.Keys.TrackKey;
import pl.geoportal.persistence.DTO.SimplifiedTimeTable;
import pl.geoportal.persistence.DTO.TimetableKey;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@Component
public class Dijkstra {

    private final SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
    private final double COST_1 = 2.0;
    private final double COST_2 = 2.3;
    private final double COST_3 = 2.6;

    public void computePathsCost(Vertex start, Integer end) {

        start.wage = 0.0;
        PriorityQueue<Vertex> vertexQueue = new PriorityQueue<>(
                20, (o1, o2) -> Double.compare(o1.wage, o2.wage)
        );

        vertexQueue.add(start);

        final Double cost2cost1 = COST_2 - COST_1;
        final Double cost3cost2 = COST_3 - COST_2;

        while (!vertexQueue.isEmpty()) {
            Vertex parent = vertexQueue.poll();
            if (Objects.equals(parent.id, end)) break;

            // Visit each edge exiting parent
            for (Edge e : parent.adjacencies) {
                Vertex child = e.target;
                Cost actualCost = null;
                if (parent.gid != child.gid) {
                    if (e.route.equals(parent.route)) {
                        if (parent.stops > 2) {
                            actualCost = new Cost(parent.wage + 0.0, false, e.route, false, true);
                        } else if (parent.stops == 0) {
                            actualCost = new Cost(parent.wage + COST_1, false, e.route, false, true);
                        } else if (parent.stops == 1) {
                            actualCost = new Cost(parent.wage + cost2cost1, false, e.route, false, true);
                        } else if (parent.stops == 2) {
                            actualCost = new Cost(parent.wage + cost3cost2, false, e.route, false, true);
                        }
                    } else {
                        actualCost = new Cost(parent.wage + COST_2, true, e.route, true, true);
                    }
                } else {
                    if (e.route.equals(parent.route)) {
                        actualCost = new Cost(parent.wage + 0.0, false, e.route, false, false);
                    } else {
                        actualCost = new Cost(parent.wage + COST_1, true, e.route, false, false);
                    }
                }

                if (actualCost == null) continue;
                double distanceThroughU = actualCost.cost;

                if (distanceThroughU < child.wage) {
                    vertexQueue.remove(child);
                    if (actualCost.transfer) {
                        child.stops = (actualCost.startFromC2) ? 1 : 0;
                        child.route = actualCost.route;
                    } else {
                        if (actualCost.changingRegion) {
                            ++child.stops;
                        }
                    }
                    child.wage = distanceThroughU;
                    child.previous = parent;
                    vertexQueue.add(child);
                }
            }
        }

    }

    public void computePathsTime(Vertex start, String startTime,
                                 Map<TimetableKey, List<SimplifiedTimeTable>> timeTables,
                                 Set<TrackKey> trackKeys, Integer end) throws ParseException {

        start.wage = 0.0;
        start.choosenTime = sdf.parse(startTime).getTime();
        PriorityQueue<Vertex> vertexQueue = new PriorityQueue<>(
                20, (o1, o2) -> Double.compare(o1.wage, o2.wage)
        );
        vertexQueue.add(start);

        while (!vertexQueue.isEmpty()) {
            Vertex parent = vertexQueue.poll();
            if (Objects.equals(parent.id, end)) break;
            // Visit each edge exiting parent
            for (Edge e : parent.adjacencies) {
                if (e.isExt || e.weight.equals(Long.MAX_VALUE) ) continue;
                Vertex child = e.target;

                final List<SimplifiedTimeTable> times = timeTables.get(new TimetableKey(child.id, 1)) != null ?
                        timeTables.get(new TimetableKey(child.id, 1))
                                .stream().filter(t -> t.getRouteId().equals(e.route))
                                .collect(Collectors.toList())
                        : null;

                Optional<SimplifiedTimeTable> ordered = prepareTimes(
                        trackKeys,
                        times,
                        parent,
                        child, new Date(parent.choosenTime + e.weight)
                );

                if (!ordered.isPresent()) continue;
                child.choosenTime = ordered.get().getDeparture().getTime();
                child.route = ordered.get().getRouteId();

                double distanceThroughU = countTimeDistance(
                        parent.wage,
                        child.choosenTime,
                        parent.choosenTime
                );

                if (distanceThroughU < child.wage) {
                    vertexQueue.remove(child);
                    child.wage = distanceThroughU;
                    child.time = distanceThroughU;
                    child.previous = parent;
                    vertexQueue.add(child);
                }
            }
        }
    }

    private double countTimeDistance(double minDistance, long childChosenTime, long parentChosenTime) {
        return minDistance + (
                (childChosenTime > parentChosenTime) ?
                        ((childChosenTime - parentChosenTime) * 0.001) :
                        ((parentChosenTime - childChosenTime) * 0.001));
    }

    private Optional<SimplifiedTimeTable> prepareTimes(final Set<TrackKey> tracks,
                                                       final List<SimplifiedTimeTable> times,
                                                       final Vertex parent, final Vertex child,
                                                       final Date estTime) {

        if (times == null) return Optional.ofNullable(null);

        for (SimplifiedTimeTable timeTable : times) {
            if (timeTable.getDeparture().getTime() >= estTime.getTime()
                    && tracks.contains(new TrackKey(parent.id, child.id, timeTable.getDirection(), timeTable.getRouteId()))) {
                return Optional.of(timeTable);
            }
        }

        return Optional.empty();
    }

    public List<Vertex> getShortestPathTo(Vertex target) {
        List<Vertex> path = new ArrayList<>();
        for (Vertex vertex = target; vertex != null; vertex = vertex.previous) {
            path.add(vertex);
        }
        Collections.reverse(path);
        return path;
    }

    public List<Vertex> getShortestPathToByCost(Vertex target) {
        List<Vertex> path = new ArrayList<>();
        for (Vertex vertex = target; vertex != null; vertex = vertex.previous) {
            path.add(vertex);
        }
        Collections.reverse(path);
        Set<Vertex> realPath = new LinkedHashSet<>();
        for(int i = 0; i < path.size() - 1; i ++){
            Integer route = path.get(i + 1).route;
            realPath.add(path.get(i));
            Vertex actualVertex = path.get(i);
            Set<Integer> visited = new HashSet<>();
            Integer stops = 0;
            actualVertex.wage = 0.0;
            do{
                visited.add(actualVertex.id);
                Optional<Edge> nextOne = actualVertex.adjacencies
                        .stream()
                        .filter(edge -> edge.route.equals(route) && !edge.isExt && !visited.contains(edge.target.id))
                        .findFirst();
                if(nextOne.isPresent()){
                    Vertex next = nextOne.get().target;
                    if(next.gid != actualVertex.gid) {
                        if(stops == 0) {
                            next.wage = actualVertex.wage + COST_2;
                            stops +=2;
                        }else if(stops == 1){
                            next.wage = actualVertex.wage + (COST_2 - COST_1);
                            ++stops;
                        }else if(stops == 2){
                            next.wage = actualVertex.wage + (COST_3 - COST_2);
                            ++stops;
                        }else if(stops > 2){
                            next.wage = actualVertex.wage + 0.0;
                        }
                    }else{
                        if(stops == 0){
                            next.wage = actualVertex.wage + COST_1;
                            ++stops;
                        }else {
                            next.wage = actualVertex.wage + 0.0;
                        }
                    }
                    actualVertex = next;
                    actualVertex.route = route;
                    realPath.add(actualVertex);
                }else{
                    path.get(i + 1).wage = actualVertex.wage + 0.0;
                    actualVertex = path.get(i + 1);
                    actualVertex.route = route;
                    realPath.add(actualVertex);
                }
            }while (actualVertex.id != path.get(i + 1).id);
        }

        return new ArrayList<>(realPath);
    }

    public double countMaxTimeFromCostPath(List<Vertex> route, Map<TimetableKey, List<SimplifiedTimeTable>> timeTables,
                                           Set<TrackKey> trackKeys, String startTime) throws ParseException {
        double maxTime = 0;
        Double noTimePenalty = 1200.0;
        Date chosenTime = sdf.parse(startTime);

        Calendar cal = Calendar.getInstance();

        for (int i = 0; i < route.size() - 1; ++i) {
            Vertex parent = route.get(i);
            Vertex child = route.get(i + 1);
            Optional<Edge> optWeight = parent.adjacencies
                    .stream().filter(p -> p.target.id == child.id)
                    .findFirst();

            cal.setTime(chosenTime);
            cal.add(Calendar.MILLISECOND, ((optWeight.isPresent() && optWeight.get().weight != null && optWeight.get().weight != Long.MAX_VALUE)
                    ? optWeight.get().weight.intValue() : ((int) noTimePenalty.longValue() * 1000)));

            Optional<SimplifiedTimeTable> timeTable = prepareTimes(
                    trackKeys, timeTables.get(new TimetableKey(child.id, 1)), parent, child, cal.getTime()
            );

            if (!timeTable.isPresent()) {
                maxTime += noTimePenalty;
                cal.setTime(chosenTime);
                cal.add(Calendar.SECOND, noTimePenalty.intValue());
                chosenTime = cal.getTime();
            } else {
                maxTime += (timeTable.get().getDeparture().getTime() - chosenTime.getTime()) * 0.001;
                chosenTime = timeTable.get().getDeparture();
            }
        }
        //in s
        return maxTime;
    }

    private double countMinValue(List<Vertex> path, Vertex end) {
        double min = 0;
        for (Vertex v : path) {
            min += v.wage;
            if (v.id == end.id) break;
        }

        return min;
    }

    public List<Vertex> createNewKPath(List<Vertex> activePath, double dT,
                                       double dC, double fMax, Vertex start,
                                       Vertex end, Map<Integer, Vertex> graph,
                                       Map<TimetableKey, List<SimplifiedTimeTable>> timeTables, Set<TrackKey> trackKeys) {

        Queue<Vertex> vertexPriorityQueue = new PriorityQueue<>((o1, o2) -> {
            return Double.compare(o1.fz, o2.fz);
        });

        Boolean stop = Boolean.FALSE;

        start.deltaCost = 0;
        start.fz = dT * start.time + dC * start.cost;

        vertexPriorityQueue.add(graph.get(start.id));

        while (!vertexPriorityQueue.isEmpty() && !stop) {
            Vertex min = vertexPriorityQueue.poll();

            if (min.id == end.id || new Double(min.fz).equals(Double.POSITIVE_INFINITY)) {
                stop = Boolean.TRUE;
            } else {
                for (Edge edge : min.adjacencies) {
                    if (edge.isExt) continue;
                    Vertex child = edge.target;
                    child.time = min.time + computeTimeForEdge(
                            min,
                            child,
                            edge.weight.equals(Long.MAX_VALUE) ? 1200 * 1000 : edge.weight,
                            timeTables, trackKeys
                    );
                    child.cost = min.cost + computeCostForEdge(min, child, edge.route);
                    child.deltaCost = child.cost - min.cost;
                    double actualFz = dT * child.time + dC * (child.cost - child.deltaCost);
                    if(actualFz < 0){
                        continue;
                    }
                    if (actualFz < child.fz && actualFz <= fMax) {
                        vertexPriorityQueue.remove(child);
                        child.previous = min;
                        child.fz = actualFz;
                        vertexPriorityQueue.add(child);
                    }
                }
            }
        }
        if (new Double(end.fz).equals(Double.POSITIVE_INFINITY)) {
            activePath.clear();
            return activePath;
        } else {
            activePath.clear();
            for (Vertex vertex = end; vertex != null; vertex = vertex.previous) {
                activePath.add(vertex);
            }
            Collections.reverse(activePath);
            return activePath;
        }
    }

    private Double computeTimeForEdge(Vertex start, Vertex end, Long time,
                                      Map<TimetableKey, List<SimplifiedTimeTable>> timeTables, Set<TrackKey> trackKeys) {

        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date(start.choosenTime));
        cal.add(Calendar.MILLISECOND, time.intValue());

        Optional<SimplifiedTimeTable> timeTable =
                prepareTimes(
                        trackKeys, timeTables.get(new TimetableKey(end.id, 1)), start, end, cal.getTime()
                );

        double countedTime = (timeTable.isPresent()) ?
                (timeTable.get().getDeparture().getTime() > end.choosenTime) ?
                        (timeTable.get().getDeparture().getTime() - start.choosenTime) * 0.001
                        : start.choosenTime - timeTable.get().getDeparture().getTime()
                : 1200;

        end.choosenTime = (timeTable.isPresent()) ? timeTable.get().getDeparture().getTime() : Long.MAX_VALUE;

        return countedTime;
    }

    private Double computeCostForEdge(Vertex start, Vertex end, Integer route) {
        if (route == null) return Double.POSITIVE_INFINITY;

        Double actualCost = start.cost;
        Integer actualRoute = start.route;

        if (end.gid == start.gid) {
            if (route.equals(actualRoute)) {
                if (start.stops > 2) {
                    actualCost += 0.0;
                } else if (start.stops == 0) {
                    actualCost += COST_1;
                } else if (start.stops == 1) {
                    actualCost += COST_2 - COST_1;
                } else if (start.stops == 2) {
                    actualCost += COST_3 - COST_2;
                }
                end.stops = start.stops + 1;
                end.route = route;
            } else {
                end.stops = 0;
                actualCost += COST_2;
                end.route = route;
            }
        } else {
            if (route.equals(actualRoute)) {
                actualCost += 0.0;
                end.stops = start.stops + 1;
                end.route = route;
            } else {
                end.stops = 0;
                actualCost += COST_1;
                end.route = route;
            }
        }

        return actualCost;
    }

    public double countMaxCostFromTimePath(List<Vertex> route, Vertex end) {
        double maxCost = 0;
        int theSameRoute = 1;
        for (int i = 0; i < route.size() - 1; ++i) {
            Vertex parent = route.get(i);
            Vertex child = route.get(i + 1);
            if (i == 0) parent.route = child.route;

            if (parent.gid != child.gid) {
                if (child.route.equals(parent.route)) {
                    if (theSameRoute > 2) {
                        maxCost += 0.0;
                    } else if (theSameRoute == 0) {
                        maxCost += COST_1;
                    } else if (theSameRoute == 1) {
                        maxCost += COST_2 - COST_1;
                    } else if (theSameRoute == 2) {
                        maxCost += COST_3 - COST_2;
                    }
                    child.stops = theSameRoute;
                    ++theSameRoute;
                } else {
                    theSameRoute = 0;
                    child.stops = theSameRoute;
                    maxCost += COST_2;
                }
            } else {
                if (child.route.equals(parent.route)) {
                    maxCost += 0.0;
                    child.stops = theSameRoute;
                    ++theSameRoute;
                } else {
                    theSameRoute = 0;
                    child.stops = theSameRoute;
                    maxCost += COST_1;
                }
            }
            child.cost = maxCost;
            if (end != null && end.id == child.id) break;
        }
        return maxCost;
    }
}
