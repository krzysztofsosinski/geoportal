package pl.geoportal.logic.Keys;

import lombok.Data;

@Data
public class TrackKey {

    public final int startId;
    public final int endId;
    public final int direction;
    public final int routeId;

}
