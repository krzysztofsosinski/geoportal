package pl.geoportal.dto;


import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class OsmFeaturePropertiesDTO {

    @JsonProperty("length")
    private double length;
    @JsonProperty("time")
    private double time;
}
