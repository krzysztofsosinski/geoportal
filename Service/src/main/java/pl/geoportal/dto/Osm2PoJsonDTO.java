package pl.geoportal.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class Osm2PoJsonDTO {

    @JsonProperty("type")
    private String type;

    @JsonProperty("features")
    private List<OsmFeaturesDTO> osmFeaturesDTOs;
}
