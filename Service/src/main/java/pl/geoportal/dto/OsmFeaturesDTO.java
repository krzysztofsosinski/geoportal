package pl.geoportal.dto;


import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class OsmFeaturesDTO {

    @JsonProperty("properties")
    private OsmFeaturePropertiesDTO osmFeaturesDTO;

}
